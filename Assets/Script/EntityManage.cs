﻿using UnityEngine;
using System.Collections;

public class EntityManage : MonoBehaviour {
	int MaxFox, MaxRabbit, MaxBear, MaxStag, MaxBoar, MaxWolf;
	int CurFox, CurRabbit, CurBear, CurStag, CurBoar, CurWolf;
	int[] Array = new int[6];
	int[] MaxArray = new int[6];
	int[] Level = new int[6];
	public GameObject FOX, RABBIT, BEAR, STAG, BOAR, WOLF;
	GameObject[] ObjectArray = new GameObject[6];
	float TimeCount = 13;
	// Use this for initialization
	void Start () {
		MaxFox = 5;
		MaxRabbit = 8;
		MaxBear = 2;
		MaxStag = 8 ;
		MaxBoar = 4;
		MaxWolf = 3;
		
		CurFox = 0;CurRabbit = 1;CurBear = 2;CurStag = 3;CurBoar = 4;CurWolf =5;
		MaxArray[CurFox] = MaxFox; MaxArray[CurRabbit] = MaxRabbit; MaxArray[CurBear] = MaxBear;
		MaxArray[CurStag] = MaxStag; MaxArray[CurBoar] = MaxBoar; MaxArray[CurWolf] = MaxWolf;
		ObjectArray[CurFox] = FOX; ObjectArray[CurRabbit] = RABBIT;
		ObjectArray[CurBear] = BEAR; ObjectArray[CurStag] = STAG;
		ObjectArray[CurBoar] = BOAR; ObjectArray[CurWolf] = WOLF;
		for(int i=0; i<6; i++) Array[i]=0;
	}
	
	float ran1, ran2;
	void CountAndMakeEntity(){
		
		for(int i=0; i<5; i++){
			GameObject[] ALLEntitys = GameObject.FindGameObjectsWithTag("Level"+i);
			if(ALLEntitys.Length ==0) continue;
			foreach(GameObject Entity in ALLEntitys){
				if(Entity.name.Equals("BEAR")){Array[CurBear]++; Level[CurBear]=i;}
				else if(Entity.name.Equals("FOX")){Array[CurFox]++; Level[CurFox]=i;}
				else if(Entity.name.Equals("BOAR")){Array[CurBoar]++; Level[CurBoar]=i;}
				else if(Entity.name.Equals("RABBIT")){Array[CurRabbit]++; Level[CurRabbit]=i;}
				else if(Entity.name.Equals("STAG")){Array[CurStag]++; Level[CurStag]=i;}
				else if(Entity.name.Equals("STANDARD_WOLF")){Array[CurWolf]++; Level[CurWolf]=i;}
			}
		}
		GameObject[] ALLRegenerations = GameObject.FindGameObjectsWithTag("RegenerationZone");
		
		for(int i=0; i<6; i++){
			ArrayList WanderEntitys = new ArrayList();
			
			Vector3 position;
			for(int k=Array[i]; k<MaxArray[i]; k++){
				int t = Random.Range(0,12);
					if(t%4==0){
						ran1 = Random.Range(3, 10+10*i);
						ran2 = Random.Range(3, 10+10*i);
					}else if(t%4==1){
						ran1 = Random.Range(3, 10+10*i);
						ran2 = Random.Range(-3, -10+10*i);
					}else if(t%4==2){
						ran1 = Random.Range(-3, -10+10*i);
						ran2 = Random.Range(3, 10+10*i);
					}else if(t%4==3){
						ran1 = Random.Range(-3, -10+10*i);
						ran2 = Random.Range(-3, -10+10*i);
					}
					position = ALLRegenerations[k%(ALLRegenerations.Length)].transform.position+new Vector3(ran1,0,ran2);
				while(Vector3.Distance(GameObject.Find("User").transform.position, position)<15f){
					t = Random.Range(0,12);
					if(t%4==0){
						ran1 = Random.Range(3, 10+10*i);
						ran2 = Random.Range(3, 10+10*i);
					}else if(t%4==1){
						ran1 = Random.Range(3, 10+10*i);
						ran2 = Random.Range(-3, -10+10*i);
					}else if(t%4==2){
						ran1 = Random.Range(-3, -10+10*i);
						ran2 = Random.Range(3, 10+10*i);
					}else if(t%4==3){
						ran1 = Random.Range(-3, -10+10*i);
						ran2 = Random.Range(-3, -10+10*i);
					}
					position = ALLRegenerations[k%(ALLRegenerations.Length)].transform.position+new Vector3(ran1,0,ran2);
				}
				GameObject temp = (GameObject)Instantiate(ObjectArray[i],position, ALLRegenerations[k%(ALLRegenerations.Length)].transform.rotation);
				if(temp.name.Equals("BEAR") || temp.name.Equals("BEAR(Clone)")){temp.transform.name="BEAR";}
				else if(temp.name.Equals("FOX")|| temp.name.Equals("FOX(Clone)")){temp.transform.name="FOX";}
				else if(temp.name.Equals("BOAR")|| temp.name.Equals("BOAR(Clone)")){temp.transform.name="BOAR";}
				else if(temp.name.Equals("RABBIT")|| temp.name.Equals("RABBIT(Clone)")){temp.transform.name="RABBIT";}
				else if(temp.name.Equals("STAG")|| temp.name.Equals("STAG(Clone)")){temp.transform.name="STAG";}
				else if(temp.name.Equals("STANDARD_WOLF")|| temp.name.Equals("STANDARD_WOLF(Clone)")){temp.transform.name="STANDARD_WOLF";}
			}
			Array[i]=0;
		}
		
	}
	// Update is called once per frame
	void Update () {
		TimeCount+=Time.deltaTime;
		if(TimeCount >15.0f){
			CountAndMakeEntity();
			TimeCount=0;
		}
	}
}
