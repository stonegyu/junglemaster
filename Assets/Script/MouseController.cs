﻿using UnityEngine;
using System.Collections;

public class MouseController : MonoBehaviour {
	public GameObject UserTarget;
	public GameObject RunAndWalkBtn;
	public bool RunAndWalkMode;
	Quaternion startVector;
	int ColliderCount;
	// Use this for initialization
	void Start () {
		UserTarget = GameObject.Find("UserMovePoint");
		RunAndWalkMode = true;
		RunAndWalkBtn = GameObject.Find("RunAndWalk");
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnGUI(){
		if(Input.GetMouseButtonDown(0)){
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			
			if(Physics.Raycast(ray, out hit)){
				if(hit.collider.name.Equals("ProtectRay")){
					return;	
				}
				if(!hit.collider.name.Equals("Wall")){
					UserTarget.transform.position = hit.point;
				}
				
				
			}
		}
		
	}
}
