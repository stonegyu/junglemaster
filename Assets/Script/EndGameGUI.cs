﻿using UnityEngine;
using System.Collections;

public class EndGameGUI : MonoBehaviour {
	DBDownload DBDown;
	public GUISkin RankSkin, MyRankSkin, EndGameBtnSkin, ExpSkin;
	
	public Texture2D emptyTex, fullTex, RankBackgroundTex, Rank_A_Tex, Rank_B_Tex, Rank_C_Tex, Rank_D_Tex, ExpBackgroundTex;
	float CurrentExp, CurrentScore, CurrentRank;
	float AnimationWaitTime=0, AnimationTime=0;
	bool isAnimation=false, AnimationeEnd=false;
	float rate=0;
	// Use this for initialization
	void Start () {
		audio.volume = StaticScripts.BackgroundVolume;
		DBDown=gameObject.GetComponent<DBDownload>();
		CurrentExp = (float)gameObject.GetComponent<MobileDBManager>().GetExpData();
		CurrentScore = (float)StaticScripts.CURRENTSCORE;
		CurrentRank = (float)StaticScripts.CURRENTRANK;
	}
	
	// Update is called once per frame
	void Update () {
		AnimationWaitTime+=Time.deltaTime;
		if(AnimationWaitTime>=1f &&isAnimation==false){
			isAnimation=true;
			Debug.Log(""+isAnimation);
		}
		if(isAnimation && !AnimationeEnd){
			AnimationTime+=Time.deltaTime;
			if(AnimationTime<=1.0f)	{
				CurrentExp+=CurrentScore*Time.deltaTime;
				rate = CurrentExp/StaticScripts.MAXIMUMEXP;
				
			}else{
				CurrentExp = gameObject.GetComponent<MobileDBManager>().GetExpData()+CurrentScore;
				gameObject.GetComponent<MobileDBManager>().SaveExpData((int)(CurrentExp));
				AnimationeEnd=true;	
			}
		}
	}
	void RankForm(int windownum){
		GUILayout.BeginVertical();		
		for(int i=1; i<=10; i++){
			GUILayout.BeginHorizontal();
			if(StaticScripts.SELSTAGE==1){
				int size = StaticScripts.Stage1Rank.Length/2;
				GUI.skin.label.alignment = TextAnchor.UpperCenter;
				GUILayout.Label (""+i, GUILayout.Width(Screen.width/4*0.3f));
				GUILayout.Label (""+StaticScripts.Stage1Rank[size-i,0], GUILayout.Width(Screen.width/2*0.2f));
				GUI.skin.label.alignment = TextAnchor.UpperLeft;
				GUILayout.Label (""+StaticScripts.Stage1Rank[size-i,1], GUILayout.Width(Screen.width/4*0.5f));
			}
			else if(StaticScripts.SELSTAGE==2){
				int size =StaticScripts.Stage2Rank.Length/2;
				GUI.skin.label.alignment = TextAnchor.UpperCenter;
				GUILayout.Label (""+i, GUILayout.Width(Screen.width/4*0.3f));
				GUILayout.Label (""+StaticScripts.Stage2Rank[size-i,0], GUILayout.Width(Screen.width/2*0.2f));	
				GUI.skin.label.alignment = TextAnchor.UpperLeft;
				GUILayout.Label (""+StaticScripts.Stage2Rank[size-i,1], GUILayout.Width(Screen.width/4*0.5f));
			}
			else if(StaticScripts.SELSTAGE==3){
				int size = StaticScripts.Stage3Rank.Length/2;
				GUI.skin.label.alignment = TextAnchor.UpperCenter;
				GUILayout.Label (""+i, GUILayout.Width(Screen.width/4*0.3f));
				GUILayout.Label (""+StaticScripts.Stage3Rank[size-i,0], GUILayout.Width(Screen.width/2*0.2f));	
				GUI.skin.label.alignment = TextAnchor.UpperLeft;
				GUILayout.Label (""+StaticScripts.Stage3Rank[size-i,1], GUILayout.Width(Screen.width/4*0.5f));
			}
			GUILayout.EndHorizontal();
		}
		
		
		
		GUILayout.EndVertical();
	}
	void MyRankForm(int windownum){
		GUILayout.BeginHorizontal();
		if(StaticScripts.SELSTAGE==1){
			GUI.skin.label.alignment = TextAnchor.UpperCenter;
			GUILayout.Label (StaticScripts.Stage1MyRank[2], GUILayout.Width(Screen.width/4*0.3f));
			GUILayout.Label (StaticScripts.Stage1MyRank[0], GUILayout.Width(Screen.width/2*0.2f));
			GUI.skin.label.alignment = TextAnchor.UpperLeft;
			GUILayout.Label (StaticScripts.Stage1MyRank[1], GUILayout.Width(Screen.width/4*0.5f));
		}else if(StaticScripts.SELSTAGE==2){
			GUI.skin.label.alignment = TextAnchor.UpperCenter;
			GUILayout.Label (StaticScripts.Stage2MyRank[2], GUILayout.Width(Screen.width/4*0.3f));
			GUILayout.Label (StaticScripts.Stage2MyRank[0], GUILayout.Width(Screen.width/2*0.2f));
			GUI.skin.label.alignment = TextAnchor.UpperLeft;
			GUILayout.Label (StaticScripts.Stage2MyRank[1], GUILayout.Width(Screen.width/4*0.5f));
		}else if(StaticScripts.SELSTAGE==3){
			GUI.skin.label.alignment = TextAnchor.UpperCenter;
			GUILayout.Label (StaticScripts.Stage3MyRank[2], GUILayout.Width(Screen.width/4*0.3f));
			GUILayout.Label (StaticScripts.Stage3MyRank[0], GUILayout.Width(Screen.width/2*0.2f));
			GUI.skin.label.alignment = TextAnchor.UpperLeft;
			GUILayout.Label (StaticScripts.Stage3MyRank[1], GUILayout.Width(Screen.width/4*0.5f));
		}
		GUILayout.EndHorizontal();
	}
	void RankDisplay(){
		GUI.skin = RankSkin;
		float screenWidth = Screen.width;
		float screenHeight = Screen.height;
		float windowWidth = Screen.width/3;
		float windowheight = Screen.height*0.65f;
		float windowX = (screenWidth-windowWidth-windowWidth*0.15f);
		float windowY = windowWidth*0.1f;		
		Rect windowRect = new Rect(windowX, windowY, windowWidth, windowheight);
		windowRect = GUILayout.Window(0, windowRect, RankForm, "RANK");
	}
	
	void MyRankDisplay(){
		GUI.skin = MyRankSkin;
		float screenWidth = Screen.width;
		float screenHeight = Screen.height;
		float windowWidth = Screen.width/3;
		float windowHeight = Screen.height*0.1f;
		float windowX = (screenWidth-windowWidth-windowWidth*0.15f);
		float windowY = Screen.height*0.8f;
		Rect windowRect = new Rect(windowX, windowY, windowWidth, windowHeight);
		windowRect = GUILayout.Window(1, windowRect, MyRankForm, "MYRANK");
	}
	
	void NextStageExpGUI(){
		GUI.skin = ExpSkin;
		Vector2 Exppos = new Vector2(Screen.width*0.1f+23, 205);
   		Vector2 Expsize = new Vector2(286, 18);
		
		float posX = Exppos.x ;
	    float posY = Exppos.y ;
	    GUI.BeginGroup(new Rect(posX, posY, Expsize.x, Expsize.y));
 	    GUI.DrawTexture(new Rect(0,0, Expsize.x, Expsize.y), emptyTex);
		
		GUI.Label(new Rect(0, 0, Expsize.x, Expsize.y),"("+CurrentExp+" / "+StaticScripts.MAXIMUMEXP+")");
	
		GUI.BeginGroup(new Rect(0, 0, Expsize.x * rate, Expsize.y));
		GUI.DrawTexture(new Rect(0,0, Expsize.x, Expsize.y), fullTex);
		GUI.Label(new Rect(0, 0, Expsize.x, Expsize.y),"("+CurrentExp+" / "+StaticScripts.MAXIMUMEXP+")");
		GUI.EndGroup();
	    GUI.EndGroup();	
	}
	void OnGUI(){
		
		
		
		float windowWidth = 315;
		float windowHeight = 45;
		float windowX = (Screen.width*0.1f);
		float windowY = Screen.width*0.35f;
		
		
		
		GUI.skin = EndGameBtnSkin;
		if(GUI.Button(new Rect(windowX, windowY, windowWidth, windowHeight),"TRY AGAIN")){
			if(StaticScripts.SELSTAGE==1){
				Application.LoadLevel("StageScene1");
			}else if(StaticScripts.SELSTAGE==2){
				Application.LoadLevel("StageScene2");
			}else if(StaticScripts.SELSTAGE==3){
				Application.LoadLevel("StageScene3");
			}
		}
		windowY+=windowHeight+Screen.height*0.02f;
		if(GUI.Button(new Rect(windowX, windowY, windowWidth, windowHeight),"SELECT STAGE")){
			DBDown.LoadDBData("ALLstage");
			StaticScripts.EndToChararcter=true;
			Application.LoadLevel("SelectCharacterScene");
		}
		windowY+=windowHeight+Screen.height*0.02f;
		if(GUI.Button(new Rect(windowX, windowY, windowWidth, windowHeight),"EXIT")){
			Application.Quit();
		}
		RankDisplay();
		MyRankDisplay();
		
		GUI.DrawTexture(new Rect(windowX, windowX/1.5f, 327, 204), RankBackgroundTex);
		NextStageExpGUI();
		
		
		GUI.DrawTexture(new Rect(windowX+10, 200, 306, 36), ExpBackgroundTex);
		if(StaticScripts.SELSTAGE==1){
			if(CurrentRank <=10) GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_A_Tex);
			else if(CurrentRank >10 && CurrentRank <= StaticScripts.Stage1Rank.Length *0.3f) 
				GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_B_Tex);
			else if(CurrentRank >StaticScripts.Stage1Rank.Length *0.3f && CurrentRank <= StaticScripts.Stage1Rank.Length *0.6f)
				GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_C_Tex);
			else if(CurrentRank >StaticScripts.Stage1Rank.Length *0.6f && CurrentRank <= StaticScripts.Stage1Rank.Length)
				GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_D_Tex);
		}else if(StaticScripts.SELSTAGE==2){
			if(CurrentRank <=10) GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_A_Tex);
			else if(CurrentRank >10 && CurrentRank <= StaticScripts.Stage2Rank.Length *0.3f) 
				GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_B_Tex);
			else if(CurrentRank >StaticScripts.Stage2Rank.Length *0.3f && CurrentRank <= StaticScripts.Stage2Rank.Length *0.6f)
				GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_C_Tex);
			else if(CurrentRank >StaticScripts.Stage2Rank.Length *0.6f && CurrentRank <= StaticScripts.Stage2Rank.Length)
				GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_D_Tex);
		}else if(StaticScripts.SELSTAGE==3){
			if(CurrentRank <=10) GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_A_Tex);
			else if(CurrentRank >10 && CurrentRank <= StaticScripts.Stage3Rank.Length *0.3f) 
				GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_B_Tex);
			else if(CurrentRank >StaticScripts.Stage3Rank.Length *0.3f && CurrentRank <= StaticScripts.Stage3Rank.Length *0.6f)
				GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_C_Tex);
			else if(CurrentRank >StaticScripts.Stage3Rank.Length *0.6f && CurrentRank <= StaticScripts.Stage3Rank.Length)
				GUI.DrawTexture(new Rect(windowX+20, 80, 108, 109), Rank_D_Tex);
		}
		
		
	}
}
