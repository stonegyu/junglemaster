﻿using UnityEngine;
using System.Collections;

public class ChildController : MonoBehaviour {
	public GameObject ChildSeek1, ChildSeek2;
	public GameObject Child1, Child2;
	public GameObject StageCharacter;
	public void CallSummon(){
		if(Child1 != null && Child2 != null)return;
		if(Child1 == null){
			Child1 = (GameObject)Instantiate(StageCharacter, ChildSeek1.transform.position, ChildSeek1.transform.rotation);
			Child1.transform.name = "Child1";
			Child1.transform.tag = "Child";
			return;
		}
		Child2 = (GameObject)Instantiate(StageCharacter, ChildSeek2.transform.position, ChildSeek2.transform.rotation);
		Child2.transform.name = "Child2";
		Child2.transform.tag = "Child";
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
