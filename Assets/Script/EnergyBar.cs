﻿using UnityEngine;
using System.Collections;

public class EnergyBar : MonoBehaviour {
	MovingEntity Entity;
	public Texture2D BloodEnergyEmptyTex;
    public Texture2D BloodEnergyFullTex;
	public float blood_rate;
	public bool isAttacked;
	public float Damage;
	float DisplayAttackTime = 0;
	
	float GetHitEffect;
	float targY;

	public GUISkin PointSkin;
	public GUISkin PointSkinShadow;
	// Use this for initialization
	void Start () {
		Entity = gameObject.GetComponent<MovingEntity>();
		blood_rate=gameObject.GetComponent<MovingEntity>().bloodEnergy/gameObject.GetComponent<MovingEntity>().maxbloodEnergy;
		isAttacked=false;
		DisplayAttackTime=0;
		Damage=0;
		
		targY = Screen.height /2;
	}
	
	// Update is called once per frame
	void Update () {
		blood_rate=gameObject.GetComponent<MovingEntity>().bloodEnergy/gameObject.GetComponent<MovingEntity>().maxbloodEnergy;
		if(isAttacked){
			DisplayAttackTime+=Time.deltaTime;
			if(DisplayAttackTime > 1.0f){
				DisplayAttackTime=0;
				isAttacked=false;
				Damage=0;
				targY = Screen.height /2;
			}
		}
		
	}
	public void AttackedGUI(){
		Vector2 targetPos;
    	targetPos = Camera.main.WorldToScreenPoint (transform.position);
		
		float posX = targetPos.x;
	    float posY =Screen.height-targetPos.y;
		float width = Screen.width/25;
		float height = Screen.height/25;
		if(isAttacked){
			Vector3 PointPosition = transform.position + new Vector3(0,20f,0);
			Vector3 screenPos2 = Camera.main.camera.WorldToScreenPoint (PointPosition);
			GetHitEffect += Time.deltaTime*30;
			targY -= Time.deltaTime*50;
			GUI.color = new Color (1.0f,0.0f,0.0f,1.0f - (GetHitEffect - 60) / 7);
			GUI.skin = PointSkinShadow;
			int d = (int)Damage;
			GUI.Label (new Rect (screenPos2.x+8 , targY-2, 80, 70), "-" + d.ToString());
			GUI.skin = PointSkin;
			GUI.Label (new Rect (screenPos2.x+10 , targY, 120, 120), "-" + d.ToString());
			if(((GetHitEffect - 60)/7)>=0) {
				GetHitEffect = 0;
			}
			GUI.color = Color.white;
		}
	}
	public void EnergyBarGUI(){
		Vector2 targetPos;
		GUI.color = Color.white;
    	targetPos = Camera.main.WorldToScreenPoint (transform.position);
		
		float posX = targetPos.x;
	    float posY =Screen.height-targetPos.y;
		float width = 49;
		float height = 12;
	    GUI.BeginGroup(new Rect(posX, posY, width, height));
 	    GUI.DrawTexture(new Rect(0,0, width, height), BloodEnergyEmptyTex);
		
		GUI.BeginGroup(new Rect(0, 0, width * blood_rate, height));
		GUI.DrawTexture(new Rect(0,0, width, height), BloodEnergyFullTex);
	    GUI.EndGroup();
		GUI.EndGroup();
	}
}
