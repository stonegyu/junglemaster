﻿using UnityEngine;
using System.Collections;

public class UserMovingEntity : MonoBehaviour {
	
	public Vector3 m_vVelocity;
	public Vector3 m_vHeading;
	public Vector3 m_vSide;
	public float MaximumEnergy = 10000;
	public float CurrentEnergy = 10000;
	public float m_dMass;
	public float m_dMaxSpeed ,m_dCurSpeed;
	public float m_dMaxForce;
	public int EntityLevel = 0;
	int CriticalPoint = 0;
	public float bloodEnergy, attackDamage, attackTime=0, gethitTime=0, DeathTime=0;
	public bool Evade, Pursuit, Wander, Seek, getHit, attack, Eatting;
	public GameObject AttackTarget;
	public GameObject UserMovePoint;
	
	Vector3 SeekOn(Vector3 MovePoint){
		
		m_vHeading = (MovePoint-gameObject.transform.position).normalized;
		Vector3 DesireVelocity = m_vHeading * m_dCurSpeed;
		return (DesireVelocity - gameObject.rigidbody.velocity);
	}
	Vector3 PursuitOn(GameObject evader){
		Vector3 ToEvader = evader.transform.position - gameObject.transform.position;
		double RelativeHeading = Vector3.Dot(gameObject.rigidbody.velocity.normalized,evader.rigidbody.velocity.normalized);
		if((Vector3.Dot(ToEvader,gameObject.rigidbody.velocity.normalized)>0) &&
			(RelativeHeading < -0.95)){
			return SeekOn(evader.transform.position);	
		}
		float LookAheadTime = ToEvader.magnitude/(m_dCurSpeed + evader.GetComponent<MovingEntity>().m_dMaxSpeed);
		return SeekOn(evader.transform.position + evader.rigidbody.velocity * LookAheadTime);
		
	}
	Vector3 Flee(Vector3 TargetPos){
		Vector3 DesiredVelocity = (gameObject.transform.position - TargetPos)*m_dCurSpeed;
		return (DesiredVelocity - gameObject.rigidbody.velocity);
	}
	
	Vector3 EvadeOn(GameObject pursuer){
		Vector3 ToPursuer = pursuer.transform.position - gameObject.transform.position;
		float LookAheadTime;
		LookAheadTime = ToPursuer.magnitude / 
				(m_dCurSpeed + pursuer.GetComponent<MovingEntity>().rigidbody.velocity.magnitude);
		
		return Flee(pursuer.transform.position + pursuer.rigidbody.velocity*LookAheadTime);
	}
	
	Vector3 Truncate(Vector3 Vec, float max){
		if(Vec.magnitude>max){
			Vec = Vec.normalized;
			Vec *= max;
		}
		return Vec;
	}
	
	Vector3 Calculate(){
		Vector3 m_vSteeringForce = Vector3.zero;
		if(Seek)m_vSteeringForce += SeekOn(UserMovePoint.transform.position) * 1.0f; //Seek MovePoint
		if(Pursuit)m_vSteeringForce += PursuitOn(AttackTarget) * 1.0f;
		m_vSteeringForce = Truncate(m_vSteeringForce, m_dMaxForce);
		return m_vSteeringForce;
	}
	void ManagingEnergy(Vector3 SteeringForce){
		float UsedEnergy = (SteeringForce.magnitude+m_dMass*1.0f)*
							(m_dCurSpeed+(0.5f*SteeringForce.magnitude*Time.deltaTime/
							m_dMass))*Time.deltaTime;
		CurrentEnergy-=UsedEnergy;
		if(CurrentEnergy<0) Destroy(gameObject);
	}
	
	void ChangeMode(string mode){
		//모드변경할때마다 모드별 스피드를 설정해야함.
		/*
		if(mode.Equals("Death")){
			Death = true;
			Pursuit = Evade = Wander = getHit = attack = Seek = false;
			gameObject.GetComponent<AnimationController>().AnimationUpdate();
			return;
		}
		
		if(getHit){
			if(Pursuer != null) return;
		}
		*/
		if(attack){
			if(AttackTarget.GetComponent<MovingEntity>().bloodEnergy>0) {
				return;
			}
		}
		
		
		
		if(mode.Equals("Seek")){
			Seek=true;
			Pursuit = Evade = Wander = getHit = attack =  Eatting = false;
			m_dCurSpeed=m_dMaxSpeed/2;
		}else if(mode.Equals("Pursuit")){
			Pursuit=true;
			Seek = Evade = Wander = getHit = attack =  Eatting = false;
			UserMovePoint = AttackTarget;
			m_dCurSpeed=m_dMaxSpeed;
		}else if(mode.Equals("Evade")){
			Evade=true;
			Pursuit = Seek = Wander = getHit = attack =  Eatting = false;
			m_dCurSpeed=m_dMaxSpeed;
		}else if(mode.Equals("Wander")){
			Wander=true;
			Pursuit = Evade = Seek = getHit = attack =  Eatting = false;
			m_dCurSpeed=m_dMaxSpeed/3;
		}else if(mode.Equals("GetHit")){
			Pursuit = Evade = Seek = Wander = attack =  Eatting = false;
			getHit = true;
			m_dCurSpeed=0.5f;
		}else if(mode.Equals ("Attack")){
			Pursuit = Evade = Seek = Wander = getHit = Eatting = false;
			attack = true;
			m_dCurSpeed=0.5f;
		}else if(mode.Equals("Eatting")){
			Pursuit = Evade = Seek = Wander = getHit = attack = false;
			Eatting = true;
			m_dCurSpeed = 0.0f;
		}
		//gameObject.GetComponent<AnimationController>().UserAnimationUpdate();
	}
	
	
	
	
	void PursuerDamageCalculate(bool isEvade){
		if(!isEvade){
			CriticalPoint++;
			return;
		}
		attackTime+=Time.deltaTime;
		if(AttackTarget.GetComponent<MovingEntity>().bloodEnergy<=0){
			AttackTarget.GetComponent<MovingEntity>().ChangeMode("Death");
		}
		if(attackTime>=1.0f){
			float Critical = 1.0f;
			if(CriticalPoint>0){
				Critical = Random.Range(2,4);
				CriticalPoint = 0;
			}
			AttackTarget.GetComponent<MovingEntity>().bloodEnergy -= Random.Range(attackDamage, attackDamage+3)*Critical;	
			attackTime = 0.0f;	
		}
	}
	
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		//EntityUpdate();
	}
	void OnCollisionEnter(Collision collision){
		if(collision.transform.name == "Terrain") return;
		if(collision.transform.name.Equals(AttackTarget.name)){
			ChangeMode("Attack");
			PursuerDamageCalculate(AttackTarget.transform.GetComponent<MovingEntity>().Evade); 
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(AttackTarget.transform.position - transform.position),1);
		}
	}
	
}
