﻿using UnityEngine;
using System.Collections;

public class SkillManager : MonoBehaviour {
	public GameObject EffectObj;
	public GameObject DeffenceObj;
	public bool IsOn;
	public bool EmittionOn;
	float SkillTime;
	// Use this for initialization
	void Start () {
		IsOn=false;
		SkillTime=0;
		if(StaticScripts.SELTYPE == 1) EffectObj = GameObject.Find("AttackEffect").gameObject;
		else EffectObj = GameObject.Find("Deffence").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		if(IsOn == true){
			EmittionOn = true;
			IsOn = false;
			if(StaticScripts.SELTYPE == 1) EffectObj.particleSystem.enableEmission = true;
			else EffectObj.renderer.enabled = true;
		}
		if(EmittionOn){
			SkillTime+=Time.deltaTime;
			if(SkillTime>=10.0f){
				SkillTime = 0;
				EmittionOn = false;
				if(StaticScripts.SELTYPE == 1) EffectObj.particleSystem.enableEmission = false;
				else EffectObj.renderer.enabled = false;
			}	
		}
	}
}
