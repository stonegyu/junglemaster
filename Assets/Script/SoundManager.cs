﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
	public AudioClip ButtonSound, GetItemSound, UpRankSound;
	public AudioClip BearEattingSound, WolfEattingSound, BoarEattingSound, FoxEattingSound;
	public AudioClip BearDeathSound, WolfDeathSound, BoarDeathSound, FoxDeathSound, StagDeathSound;
	public AudioClip AttackSound;
	public float Volume;
	public void PlayButtonSound(){
		Volume = StaticScripts.EtcVolume;
		audio.volume = Volume;
		audio.loop=false;
		audio.clip=ButtonSound;
		audio.PlayOneShot(ButtonSound);
	}
	
	public void PlayGetItemSound(){
		Volume = StaticScripts.EtcVolume;
		audio.volume = Volume;
		audio.loop=false;
		audio.clip=ButtonSound;
		audio.PlayOneShot(GetItemSound);
	}
	
	public void PlayUpRankSound(){
		if(audio.isPlaying)return;
		Volume = StaticScripts.EtcVolume;
		audio.clip=ButtonSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(UpRankSound);
	}
	public void PlayAttackSound(){
		Volume = StaticScripts.EtcVolume;
		audio.clip=AttackSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(AttackSound);
	}
	public void AnimalEattingSound(string name){
		Volume = StaticScripts.EtcVolume;
		if(name.Equals("BEAR"))PlayBearEattingSound();
		else if(name.Equals("FOX"))PlayFoxEattingSound();
		else if(name.Equals("BOAR"))PlayBoarEattingSound();
		else if(name.Equals("STANDARD_WOLF"))PlayWolfEattingSound();
		else if(name.Equals("User")){
			if(StaticScripts.SELSTAGE==1)PlayBearEattingSound();
			else if(StaticScripts.SELSTAGE==2)PlayBoarEattingSound();
			else if(StaticScripts.SELSTAGE==3)PlayFoxEattingSound();
		}
		
	}
	public void AnimalDeathSound(string name){
		Volume = StaticScripts.EtcVolume;
		Debug.Log(""+name);
		if(name.Equals("BEAR"))PlayBearDeathSound();
		else if(name.Equals("FOX"))PlayFoxDeathSound();
		else if(name.Equals("BOAR"))PlayBoarDeathSound();
		else if(name.Equals("STANDARD_WOLF"))PlayWolfDeathSound();
		else if(name.Equals("User")){
			if(StaticScripts.SELSTAGE==1)PlayBearDeathSound();
			else if(StaticScripts.SELSTAGE==2)PlayBoarDeathSound();
			else if(StaticScripts.SELSTAGE==3)PlayFoxDeathSound();
		}
		else if(name.Equals("STAG"))PlayStagDeathSound();
	}
	
	void PlayBearDeathSound(){
		Volume = StaticScripts.EtcVolume;
		audio.clip=BearDeathSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(BearDeathSound);
	}
	void PlayWolfDeathSound(){
		Volume = StaticScripts.EtcVolume;
		audio.clip=WolfDeathSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(WolfDeathSound);
	}
	void PlayBoarDeathSound(){
		Volume = StaticScripts.EtcVolume;
		audio.clip=BoarDeathSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(BoarDeathSound);
	}
	void PlayFoxDeathSound(){
		Volume = StaticScripts.EtcVolume;
		audio.clip=FoxDeathSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(FoxDeathSound);
	}
	void PlayStagDeathSound(){
		Volume = StaticScripts.EtcVolume;
		audio.clip=StagDeathSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(StagDeathSound);
	}
	
	void PlayBearEattingSound(){
		Volume = StaticScripts.EtcVolume;
		audio.clip=ButtonSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(BearEattingSound);
	}
	void PlayWolfEattingSound(){
		Volume = StaticScripts.EtcVolume;
		audio.clip=ButtonSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(WolfEattingSound);
	}
	void PlayBoarEattingSound(){
		Volume = StaticScripts.EtcVolume;
		audio.clip=ButtonSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(BoarEattingSound);
	}
	void PlayFoxEattingSound(){
		Volume = StaticScripts.EtcVolume;
		audio.clip=ButtonSound;
		audio.volume = Volume;
		audio.loop=false;
		audio.PlayOneShot(FoxEattingSound);
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
