﻿using UnityEngine;
using System.Collections;

public class BackgroundSoundManager : MonoBehaviour {
	public AudioClip CharacterBackgroundMusic;
	public AudioClip GameBackgroundMusic;
	public float Volume;
	public void PlayCharacterBackgroundMusic(){
		Volume = StaticScripts.BackgroundVolume;
		audio.volume = Volume;
		audio.loop=true;
		audio.clip=CharacterBackgroundMusic;
		audio.Play();
	}
	public void PlayGameBackgroundMusic(){
		Volume = StaticScripts.BackgroundVolume;
		audio.volume = Volume;
		audio.loop=true;
		audio.clip=GameBackgroundMusic;
		audio.Play();
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
