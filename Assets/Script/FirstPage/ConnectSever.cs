﻿using UnityEngine;
using System.Collections;
using LitJson;
using System;
using System.Text;
using System.Threading;
public class ConnectSever : MonoBehaviour
{
	// Use this for initialization
	public int downloadOK=0;
	public string[,] stage1Rank, stage2Rank, stage3Rank;
	string[,] rank;
	
	void Start(){
		
		
	}
	void Update(){
	}
	private WWW GET(string url){
		WWW www = new WWW(url);
		while(true){
			if(www.isDone==true){
				break;
			}
		}
		return www;
	}
	
	public string RegisterID(string id, int score){
		for(int i=1; i<=3; i++){
			
			//byte[] defaultBytes = Encoding.Default.GetBytes("http://www.xargus.net/lim/insert.php/?qry=INSERT%20INTO%20`stage"+i+"`(`id`,%20`score`)%20VALUES%20(%27"+id+"%27,%27"+0+"%27)");  
			//byte[] utf8Bytes = Encoding.Convert( Encoding.Default, Encoding.UTF8, defaultBytes );  
			//string url = Encoding.UTF8.GetString( utf8Bytes );
			string url = "http://www.xargus.net/lim/insert.php/?qry=INSERT%20INTO%20`stage"+i+"`(`id`,%20`score`)%20VALUES%20(%27"+id+"%27,%27"+0+"%27)";
			WWW www = new WWW(url);
			while(true){
				if(www.isDone == true) {
					break;	
				}
			}
			if(!www.text.Equals("null")){
			}else{
				return "네트워크 상태가 불안정 하거나 이미 등록된 아이디입니다";
				break;
			}
		}
		return "환영합니다!";
		gameObject.GetComponent<MobileDBManager>().SaveData(id);
	}
	
	public void SaveScore(string id, int score, int stagenum){
		byte[] defaultBytes = Encoding.Default.GetBytes("http://www.xargus.net/lim/insert.php/?qry=UPDATE`stage"+stagenum+"`SET%20score='"+score+"'WHERE%20id='"+id+"'" );  
		byte[] utf8Bytes = Encoding.Convert( Encoding.Default, Encoding.UTF8, defaultBytes );  
		string url = Encoding.UTF8.GetString( utf8Bytes ); 
		WWW www = new WWW(url);
		while(true){
			if(www.isDone == true) {
				break;
			}
		}
	}
	public string[,] LoadRankData(string stagenumber){
		string url = "http://www.xargus.net/lim/search.php/?qry=SELECT%20`id`,%20`score`%20FROM%20`"+stagenumber+"`%20WHERE%201";
		WWW www = GET(url);
		if(www.text.Equals("null") || www.text.Length == 0) {
			string[,] temp = new string[1,1];
			return temp;
		}
		LitJson.JsonData getData = LitJson.JsonMapper.ToObject(www.text);
		rank = new string[getData.Count,2];
		for(int i=0; i<getData.Count; i++){
			rank[i,0] = getData[i]["id"].ToString();
			if(getData[i]["score"] == null){
				rank[i,1] = ""+0;
			}else{
				rank[i,1] = getData[i]["score"].ToString();
			}
		}
		Quick_Sort(rank, rank.Length/2);
		downloadOK++;
		return rank;
		
	}
	void Quick_Sort(string[,] param_data, int param_count){
		int[] rankscore = new int[param_count];
		for(int i=0; i<param_count; i++){
			rankscore[i] = System.Convert.ToInt32(param_data[i,1]);	
		}
		Quick_Recursive(param_data, rankscore, 0, param_count-1);
	}
	void Quick_Recursive(string[,] param_data, int[] score, int param_left, int param_right){
		int pivot, left_hold, right_hold;
		left_hold = param_left;
		right_hold = param_right;
		
		pivot = score[param_left];
		string pivotstring = param_data[param_left,0];
		
		while(param_left < param_right){
			while((score[param_right]>=pivot) && (param_left<param_right))param_right--;
			if(param_left != param_right){
				score[param_left] = score[param_right];
				param_data[param_left,0] = param_data[param_right,0];
				param_data[param_left,1] = param_data[param_right,1];
			}
			while((score[param_left]<=pivot) && (param_left<param_right))param_left++;
			if(param_left != param_right){
				score[param_right] = score[param_left];
				param_data[param_right,0] = param_data[param_left,0];
				param_data[param_right,1] = param_data[param_left,1];
				param_right--;
			}
		}
		param_data[param_left,1]=pivot.ToString();
		param_data[param_left,0]=pivotstring;
		score[param_left]=pivot;
		pivot=param_left;
		param_left=left_hold;
		param_right=right_hold;
		if(param_left < pivot) Quick_Recursive(param_data,score, param_left, pivot-1);
		if(param_right > pivot) Quick_Recursive(param_data,score,pivot +1, param_right);
	}
}
