﻿using UnityEngine;
using System.Collections;
using System.Text;

public class FlowScripts : MonoBehaviour {
	public Texture2D LoadingEmpty, LoadingFull, GameLOGO;
	ConnectSever ConnectServer;
	MobileDBManager MobileDB;
	string RegistID="";
	string LoadingMessage ="";
	string Message = "";
	bool IsServerNetworkOK = true;
	void GoToNextScene(){
		LoadDBData();
		if(IsServerNetworkOK) Application.LoadLevel("SelectCharacterScene");
	}
	public void LoadDBData(){
		StaticScripts.MyID=RegistID;
		StaticScripts.Stage1Rank = ConnectServer.LoadRankData("stage1");	
		StaticScripts.Stage2Rank = ConnectServer.LoadRankData("stage2");
		StaticScripts.Stage3Rank = ConnectServer.LoadRankData("stage3");
		if(StaticScripts.Stage1Rank.Length == 1 || StaticScripts.Stage2Rank.Length == 1|| StaticScripts.Stage3Rank.Length == 1){
			IsServerNetworkOK=false;
			return;
		}
		for(int i=0; i<StaticScripts.Stage1Rank.Length/2; i++){
			if(StaticScripts.Stage1Rank[i,0]==StaticScripts.MyID){
				StaticScripts.Stage1MyRank[0] = StaticScripts.Stage1Rank[i,0];
				StaticScripts.Stage1MyRank[1] = StaticScripts.Stage1Rank[i,1];
				StaticScripts.Stage1MyRank[2] = ""+(StaticScripts.Stage1Rank.Length/2-i);
				break;
			}
		}
		for(int i=0; i<StaticScripts.Stage2Rank.Length/2; i++){
			if(StaticScripts.Stage2Rank[i,0]==StaticScripts.MyID){
				StaticScripts.Stage2MyRank[0] = StaticScripts.Stage2Rank[i,0];
				StaticScripts.Stage2MyRank[1] = StaticScripts.Stage2Rank[i,1];   
				StaticScripts.Stage2MyRank[2] = ""+(StaticScripts.Stage2Rank.Length/2-i);
				break;
			}
		}
		for(int i=0; i<StaticScripts.Stage3Rank.Length/2; i++){
			if(StaticScripts.Stage3Rank[i,0]==StaticScripts.MyID){
				StaticScripts.Stage3MyRank[0] = StaticScripts.Stage3Rank[i,0];
				StaticScripts.Stage3MyRank[1] = StaticScripts.Stage3Rank[i,1];
				StaticScripts.Stage3MyRank[2] = ""+(StaticScripts.Stage3Rank.Length/2-i);
				break;
			}
		}
	}
	bool IsRegisted(){
		string ID = MobileDB.GetData();
		if(ID.Length == 0){
			
			return false;
		}
		RegistID=ID;
		return true;
	}
	// Use this for initialization
	void Start () {
		ConnectServer = gameObject.GetComponent<ConnectSever>();
		MobileDB = gameObject.GetComponent<MobileDBManager>();
		if(IsRegisted()){
			GoToNextScene();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	bool id_check(char text) {
		char c = text;
		if((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) {
			
			return (false);             
		}
		return true;
	}
	
	void RegistForm(int windownum){
		
		float width = Screen.width/2;
		float height = Screen.height/5;
		float x = Screen.width/2 - width/2;
		float y = Screen.height/2 - height/2;
		
		GUILayout.BeginVertical();
		
		GUILayout.BeginHorizontal();
		RegistID = GUI.TextField(new Rect(10, 20, width*0.7f, height/3),RegistID,25);
		if(GUI.Button(new Rect(width*0.7f+20, 20, width*0.23f, height/3),"REGIST")){
			
			if(RegistID.Length >10 || RegistID.Length==0){
				Message = "ID는 1글자 이상 10글자 내로 등록가능합니다";
			}else{
				bool idcheck=true;
				byte[] defaultBytes = Encoding.Default.GetBytes(RegistID);
				char[] defaultChars = new char[defaultBytes.Length];
				
				for(int i=0; i<defaultBytes.Length;i++){
					defaultChars[i] = System.Convert.ToChar(defaultBytes[i]);
					if(id_check(defaultChars[i])==false){
						
						idcheck=false;
						break;
					}
				}
				if(idcheck==false){
					Message = "id는 공백없이 영어와 숫자만 가능합니다";
				}else{
					Message = ConnectServer.RegisterID(RegistID,0);
					if(Message.Equals("환영합니다!")){
						MobileDB.SaveData(RegistID);
						GoToNextScene();
					}
				}
			}
			
		}
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		if(Message.Length==0)Message = "등록하실 ID를 입력해주세요.(영어,숫자 가능)";
		GUI.Label(new Rect(10, 50, width, height/3),Message);
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
	}
	void LoadingInformForm(int windownum){
		
		
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		if(!IsServerNetworkOK) {
			LoadingMessage = "            네트워크가 원활하지 않습니다";
			GUILayout.Label (LoadingMessage, GUILayout.Width(Screen.width/2*0.7f));
			if(GUILayout.Button("재접속",GUILayout.Width(Screen.width/2*0.2f))){
				IsServerNetworkOK=true;
				GoToNextScene();
			}
		}
		else {
			LoadingMessage = "                  현재 "+(ConnectServer.downloadOK/3*100)+" %완료 되었습니다";
			GUILayout.Label (LoadingMessage, GUILayout.Width(Screen.width/2*0.8f));
		}
		
		
		GUILayout.EndHorizontal();
		GUILayout.EndVertical();
	}
	void LoadingInform(){
		float windowWidth = Screen.width/2;
		float windowheight = Screen.height/27;
		float windowX =  Screen.width/2-Screen.width/4;
		float windowY = Screen.height/2-Screen.height/60-Screen.height/7;		
		Rect windowRect = new Rect(windowX, windowY+100, windowWidth, windowheight);
		windowRect = GUILayout.Window(0, windowRect, LoadingInformForm, "LoadinInformation");
	}
	void OnGUI(){
		GUI.DrawTexture(new Rect((Screen.width-638)/2, 30, 638,208),GameLOGO);
		if(IsRegisted()){
			LoadingInform();
			float rate = ConnectServer.downloadOK/3;
			float width = Screen.width/2;
			float height = Screen.height/30;
			float posX = Screen.width/2-width/2;
		    float posY =Screen.height/2+100;
		    GUI.BeginGroup(new Rect(posX, posY, width, height));
	 	    GUI.DrawTexture(new Rect(0,0, width, height), LoadingEmpty);
			
			GUI.BeginGroup(new Rect(0, 0, width * rate, height));
			GUI.DrawTexture(new Rect(0,0, width, height), LoadingFull);
		    GUI.EndGroup();
			GUI.EndGroup();
			
			
		}else{
			float width = Screen.width/2;
			float height = Screen.height/5;
			float x = Screen.width/2 - width/2;
			float y = Screen.height/2 - height/2;
			Rect windowRect = new Rect(x, y+50, width, height);
			windowRect = GUILayout.Window(0, windowRect, RegistForm, "REGISTER FOR JUNGLE");
		}
		
	}
	
}
