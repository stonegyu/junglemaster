﻿using UnityEngine;
using System.Collections;


public class MobileDBManager : MonoBehaviour {
	string MyID;
	public void SaveData(string id){
		PlayerPrefs.SetString("MyID", id);
	}
	public string GetData(){
		MyID = PlayerPrefs.GetString("MyID","");
		return MyID;
	}
	public void SaveExpData(int NextQwestExp){
		PlayerPrefs.SetInt("NextQwestExp",NextQwestExp);
	}
	public int GetExpData(){
		return PlayerPrefs.GetInt("NextQwestExp",0);	
	}
	public void SaveBackGroundVolume(float volume){
		PlayerPrefs.SetFloat("BackGroundVolume",volume);	
	}
	public void SaveEtcVolume(float volume){
		PlayerPrefs.SetFloat("EtcVolume",volume);	
	}
	public float GetBackGroundVolume(){
		return PlayerPrefs.GetFloat("BackGroundVolume",50f);	
	}
	public float GetEtcVolume(){
		return PlayerPrefs.GetFloat("EtcVolume",50f);	
	}
}
