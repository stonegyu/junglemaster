﻿using UnityEngine;
using System.Collections;

public class ScoreEffect : MonoBehaviour {

	ArrayList point = new ArrayList();
	float GetHitEffect;
	float targY;
	Vector3 PointPosition;
	bool isOn;

	public GUISkin PointSkin;
	public GUISkin PointSkinShadow;
	// Use this for initialization
	void Start () {
		isOn=false;
		PointPosition = GameObject.Find("User").transform.position + new Vector3(-2f,0,0);
		targY = Screen.height /2;
	}
	void OnGUI() {
		if(point.Count>0) isOn=true;
		if(isOn){
			PointPosition = GameObject.Find("User").transform.position + new Vector3(-2f,0,0);
			Vector3 screenPos2 = Camera.main.camera.WorldToScreenPoint (PointPosition);
			GetHitEffect += Time.deltaTime*30;
			GUI.color = new Color (0.0f,1.0f,1.0f,1.0f - (GetHitEffect - 60) / 7);
			GUI.skin = PointSkinShadow;
			GUI.Label (new Rect (screenPos2.x+8 , targY-2, 80, 70), "+" + point[0].ToString());
			GUI.skin = PointSkin;
			GUI.Label (new Rect (screenPos2.x+10 , targY, 120, 120), "+" + point[0].ToString());
			if(((GetHitEffect - 60)/7)>=0) {
				RemovePoint();
			}
		}
	}
	void RemovePoint(){
		if(point.Count==0)return;
		Debug.Log(""+point.Count);
		point.RemoveAt(0);
		isOn = false;
		GetHitEffect = 0;
		targY = Screen.height /2;
	}
	public void ScoreEffectOn(int point){
		this.point.Add(point);
		targY = Screen.height /2;
	}
	// Update is called once per frame
	void Update () {
		if(isOn) targY -= Time.deltaTime*100;
	}
}
