﻿using UnityEngine;
using System.Collections;

public class GameMessage : MonoBehaviour {

	ArrayList MessageList = new ArrayList();
	ArrayList SoundList = new ArrayList();
	float DisplayTime=0;
	public Texture2D TextBackgroud;
	bool MessageSoundOneShot = false;
	public GUISkin messageskin;
	public void AddMessage(string message, string SoundName){
		MessageList.Add(message);
		SoundList.Add(SoundName);
	}
	void RemoveMessage(){
		if(MessageList.Count==0) return;
		MessageList.RemoveAt(0);
		SoundList.RemoveAt(0);
		MessageSoundOneShot=true;
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(MessageSoundOneShot && SoundList.Count>0){
			if(((string)SoundList[0]).Equals("UpRankSound")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayUpRankSound();
			}else if(((string)SoundList[0]).Equals("GetItemSound")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayGetItemSound();
			}
			
			MessageSoundOneShot=false;
		}
		if(MessageList.Count>0){
			DisplayTime+=Time.deltaTime;
			if(DisplayTime>=1.5f){
				DisplayTime=0;
				RemoveMessage();
				
			}
		}
	}
	public void MessageGUI(){
		GUI.skin=messageskin;
		if(MessageList.Count>0){
			GUI.skin.label.alignment = TextAnchor.UpperCenter;
			GUI.DrawTexture(new Rect(Screen.width/2-Screen.width/4,Screen.height/2-Screen.height/4,Screen.width/2,Screen.height/20),TextBackgroud);
			GUI.Label(new Rect(Screen.width/2-Screen.width/4,Screen.height/2-Screen.height/4,Screen.width/2,Screen.height/20),(string)MessageList[0]);
		}
	}
}
