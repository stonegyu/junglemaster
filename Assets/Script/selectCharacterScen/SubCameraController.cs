﻿using UnityEngine;
using System.Collections;

public class SubCameraController : MonoBehaviour {
	public GameObject[] StageAnimal = new GameObject[3];
	public GameObject[] StagePos = new GameObject[3];
	public int Stagenum;
	// Use this for initialization
	void Start () {
		Stagenum=0;
	}
	
	// Update is called once per frame
	public void ToRight(){
		
		Stagenum++;
		if(Stagenum==3)Stagenum=0;
		gameObject.transform.position = StagePos[Stagenum].transform.position;
		gameObject.transform.rotation = StagePos[Stagenum].transform.rotation;
		
	}
	public void ToLeft(){
		Stagenum--;
		if(Stagenum==-1)Stagenum=2;
		gameObject.transform.position = StagePos[Stagenum].transform.position;
		gameObject.transform.rotation = StagePos[Stagenum].transform.rotation;
	}
	void Update () {
		gameObject.transform.RotateAround(StageAnimal[Stagenum].transform.position,Vector3.up, 10*Time.deltaTime);
	}
}
