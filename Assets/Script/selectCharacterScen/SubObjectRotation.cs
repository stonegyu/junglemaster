﻿using UnityEngine;
using System.Collections;

public class SubObjectRotation : MonoBehaviour {
	public GameObject StageAnimal;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.RotateAround(StageAnimal.transform.position,Vector3.up, 10*Time.deltaTime);
	}
}
