﻿using UnityEngine;
using System.Collections;

public class SelectAnimalBar : MonoBehaviour {
	public Texture2D BarTexture, animalDisplayWindow, LOGO;
	Vector2 BarStartPoint;
	Vector2 Barsize;
	public GameObject EnergyBar, SpeedBar, StaminaBar, PowerBar;
	float WidthEnergyBar, WidthSpeedBar, WidthStaminaBar, WidthPowerBar;
	float EnimationEndTime=0, LimitTime=0, MoveAnimationTime=0,MoveAnimationEndTime=0;
	bool isEnimation = true, isMoveAnimation = false;
	public GUISkin Offense, Defence, Basic, GameStart, Back, ToLeft, ToRight, Exit, Sound;
	public GUISkin normalskin, rankskin, myrankskin, explainskin, characterexplaniskin;
	public bool MainSelMode = true, StageSelMode = false,CharacterSelMode = false,VolumeMode=false;
	bool Left = false, Right = false;
	public GameObject StageCameraPos, CharacterSelPos, MainSelPos;
	int Type=1;

	bool BackGroundsoundCheck=true, EtcSoundCheck=true;
	float BackGroundValue, EtcSoundValue;
	
	// Use this for initialization
	void Start () {
		StaticScripts.SELTYPE = Type;
		BackGroundValue = gameObject.GetComponent<MobileDBManager>().GetBackGroundVolume();
		EtcSoundValue = gameObject.GetComponent<MobileDBManager>().GetEtcVolume();
		StaticScripts.EtcVolume = EtcSoundValue/(Screen.width);
		StaticScripts.BackgroundVolume = BackGroundValue/(Screen.width);
		WidthEnergyBar = 25f;
		WidthSpeedBar = 40f;
		WidthStaminaBar = 25f;
		WidthPowerBar = 40f;
		if(!StaticScripts.EndToChararcter){
			MainSelMode = true;StageSelMode = false;CharacterSelMode = false;
			
		}else{
			MainSelMode=false;
			CharacterSelMode=false;
			StageSelMode=true;
			isMoveAnimation = true;
			Right = false;
			Left = true;
			MoveAnimationEndTime = 0;
			StaticScripts.EndToChararcter=false;
		}
		BarZero();
		gameObject.GetComponent<BackgroundSoundManager>().PlayCharacterBackgroundMusic();
	}
	
	// Update is called once per frame
	void Update () {
		if(MainSelMode){ // 오른쪽 
			MoveAnimationTime+=Time.deltaTime;
			if(MoveAnimationTime>0.01f && isMoveAnimation){
				MoveAnimationEndTime+=0.01f;
				if(MoveAnimationEndTime>0.5f)isMoveAnimation=false;
				float length = CharacterSelPos.transform.position.x-StageCameraPos.transform.position.x;
				gameObject.transform.position = new Vector3(transform.position.x-length/500f*10f, transform.position.y, transform.position.z);
				float CameraLength = 0.59f;
				GameObject.Find("SubCamera1").camera.rect = new Rect(GameObject.Find("SubCamera1").camera.rect.x-CameraLength/500*10f, 0.26f, 0.41f, 0.48f);
				return;
			}
		}
		if(StageSelMode){
			if(Left){
				MoveAnimationTime+=Time.deltaTime;
				if(MoveAnimationTime>0.01f && isMoveAnimation){
					MoveAnimationEndTime+=0.01f;
					if(MoveAnimationEndTime>0.5f)isMoveAnimation=false;
					float length = CharacterSelPos.transform.position.x-StageCameraPos.transform.position.x;
					gameObject.transform.position = new Vector3(transform.position.x+length/500f*10f, transform.position.y, transform.position.z);
					float CameraLength = 0.59f;
					GameObject.Find("SubCamera1").camera.rect = new Rect(GameObject.Find("SubCamera1").camera.rect.x+CameraLength/500*10f, 0.26f, 0.41f, 0.48f);
					return;
				}
			}
			if(Right){
				MoveAnimationTime+=Time.deltaTime;
				if(MoveAnimationTime>0.01f && isMoveAnimation){
					MoveAnimationEndTime+=0.01f;
					if(MoveAnimationEndTime>0.5f)isMoveAnimation=false;
					float length = CharacterSelPos.transform.position.x-StageCameraPos.transform.position.x;
					gameObject.transform.position = new Vector3(transform.position.x-length/500f*10f, transform.position.y, transform.position.z);
					float CameraLength = 0.54f-0.1f;
					GameObject.Find("SubCamera1").camera.rect = new Rect(GameObject.Find("SubCamera1").camera.rect.x-CameraLength/500*10f, 0.26f, 0.41f, 0.48f);
					return;
				}
			}
			
		}
		if(CharacterSelMode){
			MoveAnimationTime+=Time.deltaTime;
			if(MoveAnimationTime>0.01f && isMoveAnimation){
				MoveAnimationEndTime+=0.01f;
				if(MoveAnimationEndTime>0.5f)isMoveAnimation=false;
				float length = CharacterSelPos.transform.position.x-StageCameraPos.transform.position.x;
				gameObject.transform.position = new Vector3(transform.position.x+length/500f*10f, transform.position.y, transform.position.z);
				float CameraLength = 0.54f-0.1f;
				GameObject.Find("SubCamera1").camera.rect = new Rect(GameObject.Find("SubCamera1").camera.rect.x+CameraLength/500*10f, 0.26f, 0.41f, 0.48f);
				return;
			}
			LimitTime+=Time.deltaTime;
			if(LimitTime<=1f && isEnimation){
				if(LimitTime>1.0f)isEnimation=false;
				EnergyBar.transform.localScale = new Vector3(5f, 3.5f,EnergyBar.transform.localScale.z+WidthEnergyBar*Time.deltaTime);
				SpeedBar.transform.localScale = new Vector3(5f, 3.5f, SpeedBar.transform.localScale.z+WidthSpeedBar*Time.deltaTime);
				StaminaBar.transform.localScale = new Vector3(5f, 3.5f, StaminaBar.transform.localScale.z+WidthStaminaBar*Time.deltaTime);
				PowerBar.transform.localScale = new Vector3(5f, 3.5f, PowerBar.transform.localScale.z+WidthPowerBar*Time.deltaTime);
			}
		}
	}
	void BarZero(){
		EnergyBar.transform.localScale=new Vector3(5f, 3.5f, 0f);
		SpeedBar.transform.localScale=new Vector3(5f, 3.5f, 0f);
		StaminaBar.transform.localScale=new Vector3(5f, 3.5f, 0f);
		PowerBar.transform.localScale=new Vector3(5f, 3.5f, 0f);
		LimitTime=0; EnimationEndTime=0;isEnimation=true;
	}
	
	void VolumeForm(int windownum){
		
		
		GUI.skin = normalskin;
		GUILayout.BeginVertical();
		GUILayout.Label("");
		
		GUILayout.BeginHorizontal();
		string str;
		if(BackGroundsoundCheck)str=" ON";
		else str=" OFF";
		BackGroundsoundCheck = GUILayout.Toggle(BackGroundsoundCheck, " BACKGROUND MUSIC"+str);
		GUI.enabled = BackGroundsoundCheck;
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		
		GUILayout.Label("Volume:",GUILayout.Width(Screen.width/4*0.4f));
		BackGroundValue=GUILayout.HorizontalScrollbar(BackGroundValue, 20.0f,10.0f,Screen.width);
		GUI.enabled=true;
		GUILayout.EndHorizontal();
		
		GUILayout.Label("");
		
		GUILayout.BeginHorizontal();
		if(EtcSoundCheck)str=" ON";
		else str=" OFF";
		EtcSoundCheck = GUILayout.Toggle(EtcSoundCheck, " ETC SOUND"+str);
		GUI.enabled = EtcSoundCheck;
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Volume:",GUILayout.Width(Screen.width/4*0.4f));
		EtcSoundValue=GUILayout.HorizontalScrollbar(EtcSoundValue, 20.0f,0.0f,Screen.width);
		GUI.enabled=true;
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		
		GUILayout.Label("");
		
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		
		if(GUILayout.Button("APPLY")){
			
			if(!BackGroundsoundCheck)BackGroundValue=0;
			if(!EtcSoundCheck)EtcSoundValue=0;
			gameObject.GetComponent<MobileDBManager>().SaveBackGroundVolume(BackGroundValue);
			gameObject.GetComponent<MobileDBManager>().SaveEtcVolume(EtcSoundValue);
			StaticScripts.EtcVolume = EtcSoundValue/Screen.width;
			audio.volume = StaticScripts.BackgroundVolume = BackGroundValue/Screen.width;
			Debug.Log(""+BackGroundValue/(Screen.width));
			Debug.Log(""+EtcSoundValue/(Screen.width));
		}
		if(GUILayout.Button("BACK")){
			MainSelMode=true;
			VolumeMode=false;
		}
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
	}
	
	void SoundDisplay(){
		GUI.skin = normalskin;
		float screenWidth = Screen.width;
		float screenHeight = Screen.height;
		float windowWidth = Screen.width/3;
		float windowheight = Screen.height*0.51f;
		float windowX = (screenWidth-windowWidth-30)/2;
		float windowY = (screenHeight-windowheight)/2;		
		Rect windowRect = new Rect(windowX, windowY, windowWidth, windowheight);
		windowRect = GUILayout.Window(4, windowRect, VolumeForm, "VOLUME");
	}
	void RankForm(int windownum){
		GUILayout.BeginVertical();
		
		
		for(int i=1; i<=10; i++){
			GUILayout.BeginHorizontal();
			if(StaticScripts.SELSTAGE==1){
				int size = StaticScripts.Stage1Rank.Length/2;
				GUI.skin.label.alignment = TextAnchor.UpperCenter;
				GUILayout.Label (""+i, GUILayout.Width(Screen.width/4*0.3f));
				GUILayout.Label (""+StaticScripts.Stage1Rank[size-i,0], GUILayout.Width(Screen.width/2*0.2f));
				GUI.skin.label.alignment = TextAnchor.UpperLeft;
				GUILayout.Label (""+StaticScripts.Stage1Rank[size-i,1], GUILayout.Width(Screen.width/4*0.5f));
			}
			else if(StaticScripts.SELSTAGE==2){
				int size =StaticScripts.Stage2Rank.Length/2;
				GUI.skin.label.alignment = TextAnchor.UpperCenter;
				GUILayout.Label (""+i, GUILayout.Width(Screen.width/4*0.3f));
				GUILayout.Label (""+StaticScripts.Stage2Rank[size-i,0], GUILayout.Width(Screen.width/2*0.2f));
				GUI.skin.label.alignment = TextAnchor.UpperLeft;
				GUILayout.Label (""+StaticScripts.Stage2Rank[size-i,1], GUILayout.Width(Screen.width/4*0.5f));
			}
			else if(StaticScripts.SELSTAGE==3){
				int size = StaticScripts.Stage3Rank.Length/2;
				GUI.skin.label.alignment = TextAnchor.UpperCenter;
				GUILayout.Label (""+i, GUILayout.Width(Screen.width/4*0.3f));
				GUILayout.Label (""+StaticScripts.Stage3Rank[size-i,0], GUILayout.Width(Screen.width/2*0.2f));	
				GUI.skin.label.alignment = TextAnchor.UpperLeft;
				GUILayout.Label (""+StaticScripts.Stage3Rank[size-i,1], GUILayout.Width(Screen.width/4*0.5f));
			}
			GUILayout.EndHorizontal();
		}
		
		
		
		GUILayout.EndVertical();
	}
	void MyRankForm(int windownum){
		GUILayout.BeginHorizontal();
		if(StaticScripts.SELSTAGE==1){
			GUI.skin.label.alignment = TextAnchor.UpperCenter;
			GUILayout.Label (StaticScripts.Stage1MyRank[2], GUILayout.Width(Screen.width/4*0.3f));
			GUILayout.Label (StaticScripts.Stage1MyRank[0], GUILayout.Width(Screen.width/2*0.2f));
			GUI.skin.label.alignment = TextAnchor.UpperLeft;
			GUILayout.Label (StaticScripts.Stage1MyRank[1], GUILayout.Width(Screen.width/4*0.5f));
		}else if(StaticScripts.SELSTAGE==2){
			GUI.skin.label.alignment = TextAnchor.UpperCenter;
			GUILayout.Label (StaticScripts.Stage2MyRank[2], GUILayout.Width(Screen.width/4*0.3f));
			GUILayout.Label (StaticScripts.Stage2MyRank[0], GUILayout.Width(Screen.width/2*0.2f));
			GUI.skin.label.alignment = TextAnchor.UpperLeft;
			GUILayout.Label (StaticScripts.Stage2MyRank[1], GUILayout.Width(Screen.width/4*0.5f));
		}else if(StaticScripts.SELSTAGE==3){
			GUI.skin.label.alignment = TextAnchor.UpperCenter;
			GUILayout.Label (StaticScripts.Stage3MyRank[2], GUILayout.Width(Screen.width/4*0.3f));
			GUILayout.Label (StaticScripts.Stage3MyRank[0], GUILayout.Width(Screen.width/2*0.2f));
			GUI.skin.label.alignment = TextAnchor.UpperLeft;
			GUILayout.Label (StaticScripts.Stage3MyRank[1], GUILayout.Width(Screen.width/4*0.5f));
		}
		GUILayout.EndHorizontal();
	}
	
	string CharacterMessage;
	void StageNum(int windownum){
		
		GUILayout.BeginHorizontal();
		GUI.skin.label.alignment = TextAnchor.UpperCenter;
		if(StaticScripts.SELSTAGE==1)CharacterMessage="특징 : 최상위 포식자       난이도 : ★☆☆";
		else if(StaticScripts.SELSTAGE==2)CharacterMessage="특징 : 중간 포식자       난이도 : ★★☆";
		else CharacterMessage="특징 : 하위 포식자       난이도 : ★★★";
		GUILayout.Label(""+CharacterMessage,GUILayout.Width(Screen.width/2.6f));
		GUILayout.EndHorizontal();
	}
	void StageDisplay(){
		GUI.skin = explainskin;
		
		float screenWidth = Screen.width;
		float screenHeight = Screen.height;
		float windowWidth = Screen.width/2.6f;
		float windowheight = Screen.height*0.11f;
		float windowX = screenWidth*0.1f-10;
		float windowY = windowWidth*0.18f;		
		Rect windowRect = new Rect(windowX, windowY, windowWidth, windowheight);
		windowRect = GUILayout.Window(3, windowRect, StageNum, "STAGE "+StaticScripts.SELSTAGE);
	}
	void RankDisplay(){
		GUI.skin = rankskin;
		float screenWidth = Screen.width;
		float screenHeight = Screen.height;
		float windowWidth = Screen.width/3;
		float windowheight = Screen.height*0.65f;
		float windowX = (screenWidth-windowWidth-windowWidth*0.2f);
		float windowY = windowWidth*0.2f;		
		Rect windowRect = new Rect(windowX, windowY, windowWidth, windowheight);
		windowRect = GUILayout.Window(0, windowRect, RankForm, "RANK");
	}
	
	void MyRankDisplay(){
		GUI.skin = myrankskin;
		float screenWidth = Screen.width;
		float screenHeight = Screen.height;
		float windowWidth = Screen.width/3;
		float windowHeight = Screen.height*0.1f;
		float windowX = (screenWidth-windowWidth-windowWidth*0.2f);
		float windowY = Screen.height*0.78f;
		Rect windowRect = new Rect(windowX, windowY, windowWidth, windowHeight);
		windowRect = GUILayout.Window(1, windowRect, MyRankForm, "MY RANK");
	}
	string CharacterMessage2;
	void ExplainCharacter(int windownum){
		if(Type==1){
			CharacterMessage="타입 : 공격형    특징 : 공격  ↑, 스피드  ↑, 체력  ↓, 스테미너  ↓";
			CharacterMessage2 = "스킬 사용시 기본 공격력의 2~4배 증가, 크리티컬 확률이 증가합니다";
		}
		else if(Type==2){
			CharacterMessage="타입 : 방어형    특징 : 공격  ↓, 스피드  ↓, 체력  ↑, 스테미너  ↑";
			CharacterMessage2 = "스킬 사용시 기본 방어력 2~4배 증가, 크리티컬 방어율이 증가합니다";
		}
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GUILayout.Label(""+CharacterMessage);
		GUILayout.EndHorizontal();
		GUILayout.Label(""+CharacterMessage2);
		GUILayout.EndVertical();
	}
	void ExplainDisplay(){
		GUI.skin = characterexplaniskin;
		
		float screenWidth = Screen.width;
		float screenHeight = Screen.height;
		float windowWidth = Screen.width/1.5f;
		float windowheight = Screen.height*0.11f;
		float windowX = (screenWidth-windowWidth)/2;
		float windowY = 30;		
		Rect windowRect = new Rect(windowX, windowY, windowWidth, windowheight);
		windowRect = GUILayout.Window(4, windowRect, ExplainCharacter, "캐릭터 설명");
	}
	void OnGUI(){
		if(VolumeMode){
	
			SoundDisplay();
		}
		if(MainSelMode && isMoveAnimation==false){
			GUI.DrawTexture(new Rect((Screen.width-638)/2,30,638,208),LOGO);
			float posX,posY,width,height;
			posX = Screen.width/2;
			posY = Screen.height*0.55f;
			width = 280;
			height = 49;
			GUI.skin = GameStart;
			if(GUI.Button(new Rect(posX-width/2, posY, width, height),"START")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				MainSelMode=false;
				CharacterSelMode=false;
				StageSelMode=true;
				isMoveAnimation = true;
				Right = false;
				Left = true;
				MoveAnimationEndTime = 0;
			}
			
			posY += height+5f;
			GUI.skin = Sound;
			if(GUI.Button(new Rect(posX-width/2, posY, width, height),"SOUND")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				MainSelMode=false;
				VolumeMode=true;
			}
			
			posY +=height+5f;
			GUI.skin = Exit;
			if(GUI.Button(new Rect(posX-width/2, posY, width, height),"EXIT")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				Application.Quit();
			}
			
		}
		
		if(StageSelMode && isMoveAnimation==false){
			int stageNum = GameObject.Find("SubCamera1").GetComponent<SubCameraController>().Stagenum+1;
			GUI.DrawTexture(new Rect(Screen.width*0.085f, Screen.width*0.15f, Screen.width*0.433f, Screen.width*0.3f), animalDisplayWindow);
			StageDisplay();
			GUI.skin = Back;
			if(GUI.Button(new Rect(65, 362, Screen.width/8, Screen.width/20),"NEXT")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				MainSelMode=false;
				CharacterSelMode=true;
				StageSelMode=false;
				isMoveAnimation = true;
				Left = Right = false;
				MoveAnimationEndTime = 0;
				BarZero();
				
			}
			GUI.skin = Back;
			if(GUI.Button(new Rect(240,  362, Screen.width/8, Screen.width/20),"BACK")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				MainSelMode=true;
				CharacterSelMode=false;
				StageSelMode=false;
				Left = Right = false;
				isMoveAnimation = true;
				MoveAnimationEndTime = 0;
			}
			GUI.skin = ToLeft;
			if(GUI.Button(new Rect(30, 190, 31, 61),"")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				GameObject.Find("SubCamera1").GetComponent<SubCameraController>().ToLeft();
				StaticScripts.SELSTAGE = GameObject.Find("SubCamera1").GetComponent<SubCameraController>().Stagenum+1;
			}
			GUI.skin = ToRight;
			if(GUI.Button(new Rect(Screen.width * 0.53f, 190, 31, 61),"")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				GameObject.Find("SubCamera1").GetComponent<SubCameraController>().ToRight();
				StaticScripts.SELSTAGE = GameObject.Find("SubCamera1").GetComponent<SubCameraController>().Stagenum+1;
			}
			RankDisplay();
			MyRankDisplay();
			
		}
		if(CharacterSelMode  && isMoveAnimation==false){
			ExplainDisplay();
			GUI.DrawTexture(new Rect(Screen.width*0.533f, Screen.width*0.15f, Screen.width*0.433f, Screen.width*0.3f), animalDisplayWindow);
			float posX = 200;
			float posY =  362;
			int stagenum = GameObject.Find("SubCamera1").GetComponent<SubCameraController>().Stagenum;
			posX -= Screen.width/8 + Screen.width/25;
			GUI.skin = Back;
			if(GUI.Button(new Rect(410, posY, Screen.width/8, Screen.width/20),"OFFENCE")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				BarZero();
				WidthEnergyBar = 25f;
				WidthSpeedBar = 40f;
				WidthStaminaBar = 25f;
				WidthPowerBar = 40f;
				Type = 1;
				StaticScripts.SELTYPE = Type;
			}
			posX -= Screen.width/8 + Screen.width/25;
			GUI.skin = Back;
			if(GUI.Button(new Rect(585, posY, Screen.width/8, Screen.width/20),"DEFENCE")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				BarZero();
				WidthEnergyBar = 40f;
				WidthSpeedBar = 30f;
				WidthStaminaBar = 45f;
				WidthPowerBar = 15f;
				Type=2;
				StaticScripts.SELTYPE = Type;
			}
			GUI.skin = Back;
			if(GUI.Button(new Rect(25, 362, Screen.width/8, Screen.width/20),"GAME START")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				StaticScripts.SELSTAGE = GameObject.Find("SubCamera1").GetComponent<SubCameraController>().Stagenum+1;
				StaticScripts.SELTYPE = Type;
				Application.LoadLevel("StageScene"+StaticScripts.SELSTAGE);
			}
			
			GUI.skin = Back;
			if(GUI.Button(new Rect(195,  362, Screen.width/8, Screen.width/20),"BACK")){
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
				MainSelMode=false;
				CharacterSelMode=false;
				StageSelMode=true;
				isMoveAnimation = true;
				MoveAnimationEndTime = 0;
				Left = false;
				Right =true;
				//다시 고를때 초기화
				Type=1;
				StaticScripts.SELTYPE = Type;
				WidthEnergyBar = 25f;
				WidthSpeedBar = 40f;
				WidthStaminaBar = 25f;
				WidthPowerBar = 40f;
			}
		}
		
	}
}
