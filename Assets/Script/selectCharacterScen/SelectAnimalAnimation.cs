﻿using UnityEngine;
using System.Collections;

public class SelectAnimalAnimation : MonoBehaviour {
	float AnimationNumber=0;
	float AnimationTime =0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		AnimationTime += Time.deltaTime;
		if(AnimationTime>=3.0f){
			if(AnimationNumber%9 ==0){
				gameObject.animation.Play("attack1");
			}else if(AnimationNumber%9 ==8){
				gameObject.animation.Play("attack2");
			}else if(AnimationNumber%9 ==7){
				gameObject.animation.Play("idle1");
			}else if(AnimationNumber%9==6){
				if(gameObject.name.Equals("FOX")) gameObject.animation.Play("idle2");
				else gameObject.animation.Play("eatting");
			}else if(AnimationNumber%9==5){
				gameObject.animation.Play("gethit");
			}else if(AnimationNumber%9==4){
				gameObject.animation.Play("run");	
			}else if(AnimationNumber%9==3){
				gameObject.animation.Play("walk");
			}else if(AnimationNumber%9==2){
				gameObject.animation.Play("idle2");
			}else if(AnimationNumber%9==1){
				gameObject.animation.Play("idle2");
			}
			AnimationTime=0;
			AnimationNumber++;
		}
		if(StaticScripts.SELTYPE == 1 && GameObject.Find("MainCamera").GetComponent<SelectAnimalBar>().CharacterSelMode){
			gameObject.transform.FindChild("AttackEffect").particleSystem.enableEmission = true;
			gameObject.transform.FindChild("Deffence").renderer.enabled = false;
		}else if(StaticScripts.SELTYPE == 2 && GameObject.Find("MainCamera").GetComponent<SelectAnimalBar>().CharacterSelMode){
			gameObject.transform.FindChild("AttackEffect").particleSystem.enableEmission = false;
			gameObject.transform.FindChild("Deffence").renderer.enabled = true;
		}else{
			gameObject.transform.FindChild("AttackEffect").particleSystem.enableEmission = false;
			gameObject.transform.FindChild("Deffence").renderer.enabled = false;
		}
	}
}
