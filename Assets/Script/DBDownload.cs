﻿using UnityEngine;
using System.Collections;

public class DBDownload : MonoBehaviour {
	ConnectSever ConnectServer;
	// Use this for initialization
	void Start () {
		ConnectServer = gameObject.GetComponent<ConnectSever>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void LoadDBData(string stage){
		if(stage.Equals("stage1") || stage.Equals("ALLstage")){
			StaticScripts.Stage1Rank = ConnectServer.LoadRankData("stage1");
			for(int i=0; i<StaticScripts.Stage1Rank.Length/2; i++){
				if(StaticScripts.Stage1Rank[i,0]==StaticScripts.MyID){
					StaticScripts.Stage1MyRank[0] = StaticScripts.Stage1Rank[i,0];
					StaticScripts.Stage1MyRank[1] = StaticScripts.Stage1Rank[i,1];
					StaticScripts.Stage1MyRank[2] = ""+(StaticScripts.Stage1Rank.Length/2-i);
					break;
				}
			}
		}
		if(stage.Equals("stage2") || stage.Equals("ALLstage")){
			StaticScripts.Stage2Rank = ConnectServer.LoadRankData("stage2");
			for(int i=0; i<StaticScripts.Stage2Rank.Length/2; i++){
				if(StaticScripts.Stage2Rank[i,0]==StaticScripts.MyID){
					StaticScripts.Stage2MyRank[0] = StaticScripts.Stage2Rank[i,0];
					StaticScripts.Stage2MyRank[1] = StaticScripts.Stage2Rank[i,1];   
					StaticScripts.Stage2MyRank[2] = ""+(StaticScripts.Stage2Rank.Length/2-i);
					break;
				}
			}
		}
		if(stage.Equals("stage3") || stage.Equals("ALLstage")){
			StaticScripts.Stage3Rank = ConnectServer.LoadRankData("stage3");
			for(int i=0; i<StaticScripts.Stage3Rank.Length/2; i++){
				if(StaticScripts.Stage3Rank[i,0]==StaticScripts.MyID){
					StaticScripts.Stage3MyRank[0] = StaticScripts.Stage3Rank[i,0];
					StaticScripts.Stage3MyRank[1] = StaticScripts.Stage3Rank[i,1];
					StaticScripts.Stage3MyRank[2] = ""+(StaticScripts.Stage3Rank.Length/2-i);
					break;
				}
			}
		}
	}
}
