using UnityEngine;
using System.Collections;

public class DisplayUI : MonoBehaviour {
	public float blood_rate = 0.0f;
	public float energy_rate = 0.0f;
    
    public Texture2D blood_emptyTex;
    public Texture2D blood_fullTex;
	public Texture2D backgroundEnergy, backgroundStamina;
	Vector2 bloodpos,bloodsize,energypos,energysize;
	
    public Texture2D energy_emptyTex;
    public Texture2D energy_fullTex;
	public GUISkin run, walk, Blood, Stamina, DeffenceSkillSkin, AttackSkillSkin;
	
	public GUISkin rankskin, messageskin, EnergyFontskin, basicbtnskin;
	public GameObject user;
 	void UserEnergyBar(){
		GUI.skin=EnergyFontskin;
		bloodpos = new Vector2(33f, 7f);
   		bloodsize = new Vector2(270, 17);
		energypos = new Vector2(33f, 10f);
    	energysize = new Vector2(270, 17);
		
		float posX = bloodpos.x ;
	    float posY = bloodpos.y ;
	    GUI.BeginGroup(new Rect(posX, posY, bloodsize.x, bloodsize.y));
 	    GUI.DrawTexture(new Rect(0,0, bloodsize.x-3f, bloodsize.y), blood_emptyTex);
		GUI.skin.label.alignment = TextAnchor.UpperCenter;
		GUI.Label(new Rect(0, 0, bloodsize.x, bloodsize.y),"("+user.GetComponent<MovingEntity>().bloodEnergy+" / "+user.GetComponent<MovingEntity>().maxbloodEnergy+")");
	
		GUI.BeginGroup(new Rect(0, 0, bloodsize.x * blood_rate, bloodsize.y));
		GUI.DrawTexture(new Rect(0,0, bloodsize.x, bloodsize.y), blood_fullTex);
		
		GUI.Label(new Rect(0, 0, bloodsize.x, bloodsize.y),"("+user.GetComponent<MovingEntity>().bloodEnergy+" / "+user.GetComponent<MovingEntity>().maxbloodEnergy+")");
		GUI.EndGroup();
	    GUI.EndGroup();
		
		
			
		posX = energypos.x;
		posY = energypos.y+bloodsize.y+7;
		
		GUI.BeginGroup(new Rect(posX, posY, energysize.x, energysize.y));
 	    GUI.DrawTexture(new Rect(0,0, energysize.x-3, energysize.y), energy_emptyTex);
		GUI.Label(new Rect(0, 0, energysize.x, energysize.y),"("+user.GetComponent<MovingEntity>().CurrentEnergy+" / "+user.GetComponent<MovingEntity>().MaximumEnergy+")");
		GUI.BeginGroup(new Rect(0, 0, energysize.x * energy_rate, energysize.y));
		GUI.DrawTexture(new Rect(0,0, energysize.x, energysize.y), energy_fullTex);
		
		GUI.Label(new Rect(0, 0, energysize.x, energysize.y),"("+user.GetComponent<MovingEntity>().CurrentEnergy+" / "+user.GetComponent<MovingEntity>().MaximumEnergy+")");
	    GUI.EndGroup();
	    GUI.EndGroup();
		
		GUI.DrawTexture(new Rect(0, 0, 318f ,69f), backgroundEnergy);
		//GUI.DrawTexture(new Rect(0, 31f, 293f ,31f), backgroundStamina);
		
		
	}
	public string UpperRankerID, UserID, EmptyScoreMessage;
	public int UpperRank, UserRank;
	public int UpperScore, UserScore;
	void RankForm(int windownum){
		GUILayout.BeginVertical();
		
		GUILayout.BeginHorizontal();
		GUI.skin.label.alignment = TextAnchor.UpperCenter;
		GUILayout.Label (""+UpperRank, GUILayout.Width(Screen.width/4*0.2f));
		GUILayout.Label (UpperRankerID, GUILayout.Width(Screen.width/4*0.4f));
		GUI.skin.label.alignment = TextAnchor.UpperLeft;
		GUILayout.Label (""+UpperScore, GUILayout.Width(Screen.width/4*0.4f));
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		GUI.skin.label.alignment = TextAnchor.UpperCenter;
		GUILayout.Label (""+UserRank, GUILayout.Width(Screen.width/4*0.2f));
		GUILayout.Label (UserID,GUILayout.Width(Screen.width/4*0.4f));
		GUI.skin.label.alignment = TextAnchor.UpperLeft;
		GUILayout.Label (""+UserScore, GUILayout.Width(Screen.width/4*0.4f));
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
	}
	void RankDisplay(){
		float screenWidth = Screen.width;
		float screenHeight = Screen.height;
		float windowWidth = Screen.width/4;
		float windowheight = Screen.height/5;
		float windowX = (screenWidth-windowWidth-windowWidth*0.05f);
		float windowY = windowWidth*0.03f;		
		Rect windowRect = new Rect(windowX, windowY, windowWidth, windowheight);
		GUI.skin=rankskin;
		windowRect = GUILayout.Window(0, windowRect, RankForm, "RANK");
	}
	void RunAndWalk(){
		if(gameObject.GetComponent<MouseController>().RunAndWalkMode)GUI.skin = walk;
		else GUI.skin = run;
		if(GUI.Button(new Rect(Screen.width - Screen.width/10, Screen.height - Screen.width/10, 65, 64),"")){
			gameObject.GetComponent<MouseController>().RunAndWalkMode = !gameObject.GetComponent<MouseController>().RunAndWalkMode;
		}
	}
	
	float skillTime=0;
	bool IsSkillOn = true;
	void SkillDisplay(){
		if(StaticScripts.SELTYPE == 2) GUI.skin=DeffenceSkillSkin;
		else GUI.skin = AttackSkillSkin;
		if(IsSkillOn){
			if(GUI.Button(new Rect(Screen.width - Screen.width/10, Screen.height - Screen.width/10- 65, 65, 64),"")){
				gameObject.GetComponent<SkillManager>().IsOn=true;
				IsSkillOn = false;
			}
		}
		else{
			if(GUI.Button(new Rect(Screen.width - Screen.width/10, Screen.height - Screen.width/10- 65, 65, 64),(int)(20-skillTime)+""));
		}
		
		
	}
	
	void ItemDisplay(){
		
		int stamina = user.GetComponent<ItemScripts>().Stamina;
		int Energy = user.GetComponent<ItemScripts>().Energy;
		int Count=0;
		if(stamina==0)GUI.skin=basicbtnskin;
		else if(stamina>0) GUI.skin = Stamina;
		if(GUI.Button(new Rect(Screen.width - Screen.width/10, Screen.height - Screen.width/10-(65*2), 65, 64),""+stamina)){
			if(stamina==0) return;
			if(stamina>0) {
				user.GetComponent<ItemScripts>().Stamina--;
				user.GetComponent<MovingEntity>().CurrentEnergy+=user.GetComponent<MovingEntity>().MaximumEnergy*0.1f;
				if(user.GetComponent<MovingEntity>().CurrentEnergy>user.GetComponent<MovingEntity>().MaximumEnergy)
					user.GetComponent<MovingEntity>().CurrentEnergy=user.GetComponent<MovingEntity>().MaximumEnergy;
			}
		}
		if(Energy==0)GUI.skin=basicbtnskin;
		else if(Energy>0)GUI.skin = Blood;		
		if(GUI.Button(new Rect(Screen.width - Screen.width/10f, Screen.height - Screen.width/10-(65*3), 65, 64),""+Energy)){
			if(Energy==0) return;
			if(Energy>0) {
				user.GetComponent<ItemScripts>().Energy--;
				user.GetComponent<MovingEntity>().bloodEnergy+=user.GetComponent<MovingEntity>().maxbloodEnergy*0.1f;
				if(user.GetComponent<MovingEntity>().bloodEnergy>user.GetComponent<MovingEntity>().maxbloodEnergy)
					user.GetComponent<MovingEntity>().bloodEnergy=user.GetComponent<MovingEntity>().maxbloodEnergy;
			}
		}
	}

    void OnGUI() {
 		
		for(int i=0; i<5; i++){
			GameObject[] ALLPredetors = GameObject.FindGameObjectsWithTag("Level"+i);
			if(ALLPredetors.Length ==0) continue;
			
			foreach(GameObject Predetor in ALLPredetors){
				
				if(Predetor.GetComponent<EnergyBar>().isAttacked){
					if(Predetor.GetComponent<MovingEntity>().Pursuer != null && Predetor.GetComponent<MovingEntity>().Pursuer.name.Equals("User"))
						Predetor.GetComponent<EnergyBar>().AttackedGUI();
					if(Predetor.GetComponent<MovingEntity>().Evader != null && Predetor.GetComponent<MovingEntity>().Evader.name.Equals("User"))
						Predetor.GetComponent<EnergyBar>().AttackedGUI();
				}
				if(!Predetor.name.Equals("User")){
					Predetor.GetComponent<EnergyBar>().EnergyBarGUI();
				}else{
					Predetor.GetComponent<EnergyBar>().AttackedGUI();
				}
				
			}
		}
		GUI.skin = null;
		
		UserEnergyBar();
		RankDisplay();
		RunAndWalk();
		
		GUI.skin=messageskin;
		ItemDisplay();
		SkillDisplay();
		gameObject.GetComponent<QwestScripts>().QwestGUI();
		gameObject.GetComponent<GameMessage>().MessageGUI();
    }
	void Start () {
		Screen.SetResolution(800, 480, true);
		user = GameObject.Find("User");
		gameObject.GetComponent<BackgroundSoundManager>().PlayGameBackgroundMusic();
		
	}
	
	// Update is called once per frame
	void Update () {
		blood_rate = user.GetComponent<MovingEntity>().bloodEnergy/user.GetComponent<MovingEntity>().maxbloodEnergy;
		energy_rate = user.GetComponent<MovingEntity>().CurrentEnergy/user.GetComponent<MovingEntity>().MaximumEnergy;
		if(IsSkillOn == false){
			skillTime += Time.deltaTime;
			if(skillTime>=20f) {
				IsSkillOn = true;
				skillTime=0;
			}	
		}
	}
}
