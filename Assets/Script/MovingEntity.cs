﻿using UnityEngine;
using System.Collections;

public class MovingEntity : MonoBehaviour {
	public Vector3 m_vVelocity;
	public Vector3 m_vHeading;
	public Vector3 m_vSide;
	public float MaximumEnergy = 10000;
	public float CurrentEnergy = 10000;
	public float m_dMass;
	public float m_dMaxSpeed, m_dCurSpeed;
	public float EatEnergy, BloodEatEnergy;
	public float m_dMaxForce;
	public int EntityLevel = 0;
	public bool Evade, Pursuit, Wander, Seek;
	public GameObject SeekPoint;
	public GameObject Pursuer;
	public GameObject Evader;
	public GameObject NearSameSpecies;
	float m_dWanderJitter = 80.0f;
	float WanderRadius    = 1.2f;
	float WanderDistance   = 2.0f;
	public float WanderTime = 0.0f; // 주위 탐색 시간 정하기
	float WanderMaxTime= 0;
	public bool attack = false, getHit = false, Eatting = false, Death = false;
	public float maxbloodEnergy,bloodEnergy, attackDamage, attackTime=0, gethitTime=0, DeathTime=0;
	int CriticalPoint = 0;
	float distanceToAnimal, nearestAnimal;
	
	public GameObject CriticalEffact;
	
	void GetHit(GameObject Pursuer){
		//return SeekOn(Pursuer.transform.position);
	}
	Vector3 SeekOn(Vector3 MovePoint){
		m_vHeading = (MovePoint-gameObject.transform.position).normalized;
		Vector3 DesireVelocity = m_vHeading * m_dCurSpeed;
		return (DesireVelocity - gameObject.rigidbody.velocity);
	}
	
	Vector3 Flee(Vector3 TargetPos){
		Vector3 DesiredVelocity = (gameObject.transform.position - TargetPos)*m_dCurSpeed;
		return (DesiredVelocity - gameObject.rigidbody.velocity);
	}
	
	Vector3 EvadeOn(GameObject pursuer){
		Vector3 ToPursuer = pursuer.transform.position - gameObject.transform.position;
		float LookAheadTime;
			LookAheadTime = ToPursuer.magnitude / 
				(m_dCurSpeed + pursuer.GetComponent<MovingEntity>().rigidbody.velocity.magnitude);
		
		return Flee(pursuer.transform.position + pursuer.rigidbody.velocity*LookAheadTime);
	}
	
	Vector3 PursuitOn(GameObject evader){
		Vector3 ToEvader = evader.transform.position - gameObject.transform.position;
		double RelativeHeading = Vector3.Dot(gameObject.rigidbody.velocity.normalized,evader.rigidbody.velocity.normalized);
		if((Vector3.Dot(ToEvader,gameObject.rigidbody.velocity.normalized)>0) &&
			(RelativeHeading < -0.95)){
			return SeekOn(evader.transform.position);	
		}
		
		float LookAheadTime = ToEvader.magnitude/(m_dCurSpeed + evader.GetComponent<MovingEntity>().m_dMaxSpeed);
		return SeekOn(evader.transform.position + evader.rigidbody.velocity * LookAheadTime);
		
	}
	Vector3 WanderOn(){
		if(WanderTime<WanderMaxTime){
			WanderTime +=Time.deltaTime;
			return Vector3.zero;
		}else{
			WanderTime=0;
			gameObject.GetComponent<AnimationController>().WanderCount++;
			float JitterThisTimeSlice = m_dWanderJitter * Time.deltaTime;
			Vector3 m_vWanderTarget = new Vector3(Random.Range(-1f,1f) * JitterThisTimeSlice,0,
												Random.Range(-1f,1f) * JitterThisTimeSlice);
			m_vWanderTarget = m_vWanderTarget.normalized;
			m_vWanderTarget *= WanderRadius;
			Vector3 targetLocal = m_vWanderTarget + new Vector3(WanderDistance, 0, 0);
			Vector3 targetWorld = gameObject.transform.TransformPoint(targetLocal);
			return (targetWorld-gameObject.transform.position);
			
		}
		
	}
	bool isDeath= false;
	void DeathOn(){
		DeathTime += Time.deltaTime;
		if(DeathTime > 2.5f && isDeath==false) {
			isDeath=true;
			if(Pursuer == null && Evader != null){ //지금 추격자가 널이면 난 희생자랑 싸우다 죽은거임
				Evader.GetComponent<MovingEntity>().Pursuer = null;
				gameObject.rigidbody.isKinematic = false;
			}
			else if(Evader == null && Pursuer!=null){ //희생자가 널이면 난 추격자랑 싸우다 죽은거임
				
				Pursuer.GetComponent<MovingEntity>().Evader = null;
				gameObject.rigidbody.isKinematic = false;
			}
			
			if(gameObject.name.Equals("User")){
				GameObject.Find("Camera").GetComponent<GameMessage>().AddMessage("사망 하였습니다", "UpRankSound");
				
				int BeforeScore=0;
				int CurrentScore = GameObject.Find ("Camera").GetComponent<ManageRanking>().CurrentGameScore;
				int CurrentRank = GameObject.Find("Camera").GetComponent<ManageRanking>().CurrentRank;
				StaticScripts.CURRENTSCORE = CurrentScore;
				StaticScripts.CURRENTRANK = CurrentRank;
				if(StaticScripts.SELSTAGE==1) BeforeScore = System.Convert.ToInt32(StaticScripts.Stage1MyRank[1]);
				else if(StaticScripts.SELSTAGE==2) BeforeScore = System.Convert.ToInt32(StaticScripts.Stage2MyRank[1]);
				else if(StaticScripts.SELSTAGE==3)BeforeScore = System.Convert.ToInt32(StaticScripts.Stage3MyRank[1]);
				if(CurrentScore>BeforeScore) gameObject.GetComponent<ConnectSever>().SaveScore(StaticScripts.MyID,CurrentScore,StaticScripts.SELSTAGE);
				if(StaticScripts.SELSTAGE==1) gameObject.GetComponent<DBDownload>().LoadDBData("stage1");
				else if(StaticScripts.SELSTAGE==2) gameObject.GetComponent<DBDownload>().LoadDBData("stage2");
				else if(StaticScripts.SELSTAGE==3)gameObject.GetComponent<DBDownload>().LoadDBData("stage3");
				Application.LoadLevel("EndGameScene");				
			}else{
				Destroy(gameObject);
			}
			
		}
		
	}
	float EattingTime=0;
	
	void EattingOn(){
		
		EattingTime += Time.deltaTime;
		
		if(Evader == null && Pursuer == null) {
			if(gameObject.transform.name.Equals("User")){
				GameObject.Find("Camera").GetComponent<ManageRanking>().UpdateScore(100);
				GameObject.Find("Camera").GetComponent<QwestScripts>().MatchAnimal(StaticScripts.deather);
			}
			if(Random.Range(0,10)<10 && gameObject.name.Equals("User")){
					gameObject.GetComponent<ItemScripts>().itemManage();
			}
			gameObject.rigidbody.isKinematic = false;
			SeekPoint.transform.position = gameObject.transform.position;
			ChangeMode("Wander");
		}
		if(EattingTime>=0.1){
			if(Pursuer == null && Evader!=null){
				if(gameObject.name.Equals("User")){
					CurrentEnergy +=  Evader.GetComponent<MovingEntity>().EatEnergy/25;
					bloodEnergy += Evader.GetComponent<MovingEntity>().BloodEatEnergy/25;
				}
				else{
					CurrentEnergy = Evader.GetComponent<MovingEntity>().MaximumEnergy;
					bloodEnergy += Evader.GetComponent<MovingEntity>().BloodEatEnergy/25;
				}
				
			}
			if(Evader == null && Pursuer !=null){
				if(gameObject.name.Equals("User")){
					CurrentEnergy +=  Pursuer.GetComponent<MovingEntity>().EatEnergy/25;
					bloodEnergy += Pursuer.GetComponent<MovingEntity>().BloodEatEnergy/25;
				}else{
					CurrentEnergy = Pursuer.GetComponent<MovingEntity>().MaximumEnergy;
					bloodEnergy += Pursuer.GetComponent<MovingEntity>().BloodEatEnergy/25;	
				}
				
			}
			if(CurrentEnergy>MaximumEnergy) CurrentEnergy = MaximumEnergy;
			if(bloodEnergy>maxbloodEnergy) bloodEnergy = maxbloodEnergy;
			
			EattingTime=0;
		}
		
		
	}
	Vector3 Truncate(Vector3 Vec, float max){
		if(Vec.magnitude>max){
			Vec = Vec.normalized;
			Vec *= max;
		}
		return Vec;
	}
	
	Vector3 Calculate(){
		if(gameObject.transform.position.y<80)Destroy(gameObject);
		Vector3 m_vSteeringForce = Vector3.zero;
		
		//포식자 정하는 알고리즘 만들어야함
		if(Seek)  m_vSteeringForce += SeekOn(SeekPoint.transform.position) * 1.0f; //Seek MovePoint
		if(Evade) m_vSteeringForce += EvadeOn(Pursuer) * 1.0f;// EvadeWeight = 1.0
		if(Pursuit) m_vSteeringForce += PursuitOn(Evader) * 1.0f;
		if(Wander)m_vSteeringForce += WanderOn() * 1.0f;
		if(getHit && Pursuer != null) GetHit(Pursuer);
		if(Death) DeathOn ();
		if(Eatting) EattingOn();
		
		m_vSteeringForce = Truncate(m_vSteeringForce, m_dMaxForce);
		return m_vSteeringForce;
	}
	
	public void ChangeMode(string mode){
		//모드변경할때마다 모드별 스피드를 설정해야함.
		if(mode.Equals("Death")){
			Death = true;
			Pursuit = Evade = Wander = getHit = attack = Seek =false;
			gameObject.GetComponent<AnimationController>().AnimationUpdate();
			return;
		}
		
		
		
		if(mode.Equals("Seek")){
			gameObject.rigidbody.isKinematic = false;
			Seek=true;
			Pursuit = Evade = Wander = getHit = attack =  Eatting = false;
			if(!gameObject.name.Equals("User")){
				if(Evader == null)SeekPoint.transform.position = gameObject.transform.position;
			}
			if(GameObject.Find("Camera").GetComponent<MouseController>().RunAndWalkMode)m_dCurSpeed=m_dMaxSpeed/2;
			else m_dCurSpeed=m_dMaxSpeed;
		}else if(mode.Equals("Pursuit")){
			gameObject.rigidbody.isKinematic = false;
			Pursuit=true;
			Seek = Evade = Wander = getHit = attack =  Eatting = false;
			m_dCurSpeed=m_dMaxSpeed;
			if(!gameObject.name.Equals("User")){
				if(Evader == null)SeekPoint.transform.position = gameObject.transform.position;
			}
		}else if(mode.Equals("Evade")){
			gameObject.rigidbody.isKinematic = false;
			Evade=true;
			Pursuit = Seek = Wander = getHit = attack =  Eatting = false;
			m_dCurSpeed=m_dMaxSpeed;
		}else if(mode.Equals("Wander")){
			gameObject.rigidbody.isKinematic = false;
			Wander=true;
			Pursuit = Evade = Seek = getHit = attack =  Eatting = false;
			m_dCurSpeed=m_dMaxSpeed/3;
		}else if(mode.Equals("GetHit")){
			Pursuit = Evade = Seek = Wander = attack =  Eatting = false;
			getHit = true;
			m_dCurSpeed=0f;
		}else if(mode.Equals ("Attack")){
			Pursuit = Evade = Seek = Wander = getHit = Eatting = false;
			attack = true;
			m_dCurSpeed=0f;
		}else if(mode.Equals("Eatting")){
			Pursuit = Evade = Seek = Wander = getHit = attack = false;
			Eatting = true;
			m_dCurSpeed = 0.0f;
		}
		gameObject.GetComponent<AnimationController>().AnimationUpdate();
	}
	
	
	float nearestPrey, nearestPredetor, distanceToPrey, distanceToPredetor, nearestsameSpecies, distanceTosameSpecies;
	void DecideAction(){
		if(Death || Eatting) return;
		
		if(attack){
			if(Evader!=null)PursuerDamageCalculate(true);
			else ChangeMode("Wander");// true는 데미지 계산 false는 critical 계산
			return;	
		}
		if(getHit){
			if(Pursuer!=null)EvaderDamageCalculate();
			else ChangeMode("Wander");
			return;
		}
		
		nearestPrey = nearestPredetor = distanceToPrey = distanceToPredetor = nearestsameSpecies = distanceTosameSpecies =9999999;
		
		//for(int i=EntityLevel; i<=4; i++){
			GameObject[] SameSpecies = GameObject.FindGameObjectsWithTag("Level"+EntityLevel);
			if(SameSpecies.Length >1){
				int Number = 0;
				if(SameSpecies[Number].name.Equals(gameObject.name)){
					Number++;
				}
				distanceTosameSpecies = Vector3.Distance (transform.position,SameSpecies[Number].transform.position);
				if(distanceTosameSpecies < nearestsameSpecies){
					nearestsameSpecies = distanceTosameSpecies;
					NearSameSpecies = SameSpecies[Number];
				}
				foreach(GameObject Species in SameSpecies){
					if(!Species.name.Equals(gameObject.name)){
						distanceTosameSpecies = Vector3.Distance(transform.position, Species.transform.position);
						if(distanceTosameSpecies < nearestsameSpecies){
							nearestsameSpecies = distanceTosameSpecies;
							NearSameSpecies = Species;
						}
					}
				}
				if(nearestsameSpecies<10.0f ){
					if((NearSameSpecies.GetComponent<MovingEntity>().Evade || NearSameSpecies.GetComponent<MovingEntity>().getHit || NearSameSpecies.GetComponent<MovingEntity>().attack)){
						if(NearSameSpecies.GetComponent<MovingEntity>().Pursuer != null){
							Pursuer = NearSameSpecies.GetComponent<MovingEntity>().Pursuer;
							ChangeMode("Evade");
							return;
						}else{
							Pursuer = null;
						}
					}
				}
				
			}
		//}
		
		
		
		//우선순위 1 : 포식자 판단
		for(int i=EntityLevel+1; i<=4; i++){
			GameObject[] ALLPredetors = GameObject.FindGameObjectsWithTag("Level"+i);
			if(ALLPredetors.Length ==0) continue;
			
			distanceToPredetor = Vector3.Distance (transform.position,ALLPredetors[0].transform.position);
			if(distanceToPredetor < nearestPredetor){
				nearestPredetor = distanceToPredetor;
				Pursuer = ALLPredetors[0];
			}
			foreach(GameObject Predetor in ALLPredetors){
				distanceToPredetor = Vector3.Distance(transform.position, Predetor.transform.position);
				if(distanceToPredetor < nearestPredetor){
					nearestPredetor = distanceToPredetor;
					Pursuer = Predetor;
				}
			}
		}
		if(Pursuer != null){
			if(nearestPredetor<15.0f){
				Vector3 forward = transform.TransformDirection(Vector3.forward);
				Vector3 toOther = Pursuer.transform.position-transform.position;
				if(Vector3.Dot(forward, toOther)>0f)ChangeMode("Evade");//시아각
				return;
			}
		}
		//우선순위 2 에너지 보유량 판단
		if(CurrentEnergy/MaximumEnergy > 0.65f){
			ChangeMode("Wander");
			return;
		}
		
		//우선순위 3 에너지 보유량이 적다면 먹이를 먹어야함
		for(int i=0; i<EntityLevel; i++){
			GameObject[] ALLPreys = GameObject.FindGameObjectsWithTag("Level"+i);
			if(ALLPreys.Length <= 0) continue;
			
			distanceToPrey = Vector3.Distance (transform.position, ALLPreys[0].transform.position);
			if(distanceToPrey < nearestPrey){
				nearestPrey = distanceToPrey;
				Evader = ALLPreys[0];
			}
			foreach(GameObject Prey in ALLPreys){
				if(Prey.GetComponent<MovingEntity>().CurrentEnergy<=0)continue;
				distanceToPrey = Vector3.Distance (transform.position, Prey.transform.position);
				if(distanceToPrey < nearestPrey){
					nearestPrey = distanceToPrey;
					Evader = Prey;
				}
			}
		}
		if(Evader != null){
			//추격 할때 행동 양식 
			//1. 내가 다가가고 있는 희생자가 도망 모드인가!?  -> 아니면 Seek로 다가감
			//2. 만약 다가가고있는 희생자가 도망모드로 변환된다면 추격모드로 전환할것인가를 판단해야함
			//3. 추격모드전환에 대한 판단은 현재 보유 에너지를 갖고 판단함.
			if(!Evader.GetComponent<MovingEntity>().Evade){
				SeekPoint.transform.position = Evader.transform.position;
				ChangeMode("Seek");
				return;
			}
			
			float PreyMaxSpeedTime = Evader.GetComponent<MovingEntity>().m_dMass * 
				Evader.GetComponent<MovingEntity>().m_dMaxSpeed/
				Evader.GetComponent<MovingEntity>().m_dMaxForce;
			float PredetorMaxSpeedTime = m_dMass*m_dMaxSpeed/m_dMaxForce;
			float Beta = PredetorMaxSpeedTime - PreyMaxSpeedTime;
			float ToEvader = Vector3.Distance(Evader.transform.position, gameObject.transform.position);
			if(Beta<0)	Beta = 0;
			float CurrentSpeed = gameObject.rigidbody.velocity.magnitude;
			float NeedEnergy = (m_dMass*0.5f)*
				(m_dMaxSpeed*m_dMaxSpeed-CurrentSpeed*CurrentSpeed)+
				((m_dMass*1.0f)* //마찰력 설정에 대한 고찰
				(m_dMaxSpeed*ToEvader)/  //시작할때 둘의 거리
				(m_dMaxSpeed-Evader.GetComponent<MovingEntity>().m_dMaxSpeed))+
				((m_dMass*1.0f*Beta*m_dMaxSpeed*Evader.GetComponent<MovingEntity>().m_dMaxSpeed*0.5f)/
				((m_dMaxSpeed-Evader.GetComponent<MovingEntity>().m_dMaxSpeed)));
			
			if(NeedEnergy<=CurrentEnergy){
				ChangeMode("Pursuit");
				
			}else{
				ChangeMode("Wander"); //희생자가 도망가는 상태이며 내가 따라가는 에너지를 보유하지 못할경우 배회하기
			}
			
		}
	}
	
	float ZeroEnergyTime=0;
	void ManagingEnergy(Vector3 SteeringForce){
		if(gameObject.tag.Equals("Level0"))return;
		float UsedEnergy = (SteeringForce.magnitude+m_dMass*1.0f)*
							(m_dCurSpeed+(0.5f*SteeringForce.magnitude*Time.deltaTime/
							m_dMass))*Time.deltaTime;
		CurrentEnergy-=UsedEnergy;
		if(CurrentEnergy<=0){
			CurrentEnergy=0;
			ZeroEnergyTime+=Time.deltaTime;
			if(ZeroEnergyTime>=1.0f){
				bloodEnergy-=maxbloodEnergy*0.05f;
				ZeroEnergyTime=0;
			}
			if(bloodEnergy<=0){
				bloodEnergy=0;
				ChangeMode("Death");
			}
		}
	}
	
	void DecideEntityAttribute(){
		if(transform.name.Equals("BEAR")){
			EntityLevel = 4;
			bloodEnergy = maxbloodEnergy = 100;
			attackDamage = 15;
			BloodEatEnergy = 15f;
			EatEnergy = 1000;
			m_dMaxSpeed = 10f;
			m_dMass = 10f;
		}else if(transform.name.Equals("STANDARD_WOLF")) {
			EntityLevel = 3;
			bloodEnergy = maxbloodEnergy = 60;
			attackDamage = 10;
			BloodEatEnergy = 10;
			EatEnergy = 100;
			m_dMaxSpeed = 8.5f;
			m_dMass = 8f;
		}else if(transform.name.Equals("BOAR")){
			EntityLevel=2;	
			bloodEnergy = maxbloodEnergy = 45;
			attackDamage = 8;
			BloodEatEnergy = 8;
			EatEnergy = 80;
			m_dMaxSpeed = 7.0f;
			m_dMass = 6f;
		}else if(transform.name.Equals("FOX")) {
			EntityLevel = 1;
			bloodEnergy = maxbloodEnergy = 40;
			attackDamage = 7;
			BloodEatEnergy = 7f;
			EatEnergy = 60;
			m_dMaxSpeed = 6.5f;
			m_dMass = 3.5f;
		}else if(transform.name.Equals("RABBIT")){
			EntityLevel = 0;
			bloodEnergy = maxbloodEnergy = 30;
			BloodEatEnergy = 2;
			EatEnergy = 40;
			m_dMaxSpeed = 5.5f;
			m_dMass = 1f;
		}else if(transform.name.Equals("STAG")){
			EntityLevel=0;	
			bloodEnergy = maxbloodEnergy = 45;
			attackDamage = 3;
			BloodEatEnergy = 3f;
			EatEnergy = 20;
			m_dMaxSpeed = 5.5f;
			m_dMass = 2.5f;
		}else if(transform.name.Equals("User")){
			float OratioEnergy=2.5f, OratioSpeed=4.0f, OratioPower=4.0f;
			float DratioEnergy=4.0f, DratioSpeed=3.0f, DratioPower=1.5f;
			float NormratioEnergy = 3.0f, NormratioSpeed=3.5f, NormratioPower=3.0f;
			if(StaticScripts.SELSTAGE == 1){
				if(StaticScripts.SELTYPE == 1){ //offence
					bloodEnergy = maxbloodEnergy = Mathf.Round(100/NormratioEnergy*OratioEnergy);
					attackDamage = 15/NormratioPower*OratioPower;
					m_dMaxSpeed = 10/NormratioSpeed*OratioSpeed;
					
				}else if(StaticScripts.SELTYPE == 2){
					bloodEnergy = maxbloodEnergy = Mathf.Round(100/NormratioEnergy*DratioEnergy);
					attackDamage = 15/NormratioPower*DratioPower;
					m_dMaxSpeed = 10/NormratioSpeed*DratioSpeed;
				}
				EntityLevel=4;
				m_dMass = 10f;
				BloodEatEnergy = 10;
				EatEnergy = 1000;
			}else if(StaticScripts.SELSTAGE == 2){
				if(StaticScripts.SELTYPE == 1){
					bloodEnergy = maxbloodEnergy = Mathf.Round(45/NormratioEnergy*OratioEnergy);
					attackDamage = 8/NormratioPower*OratioPower;
					m_dMaxSpeed = 7/NormratioSpeed*OratioSpeed;
				}else if(StaticScripts.SELTYPE == 2){
					bloodEnergy = maxbloodEnergy = Mathf.Round(45/NormratioEnergy*DratioEnergy);
					attackDamage = 8/NormratioPower*DratioPower;
					m_dMaxSpeed = 7/NormratioSpeed*DratioSpeed;
				}
				
				EntityLevel=2;
				m_dMass = 6f;
				BloodEatEnergy = 6;
				EatEnergy = 80;
			}else if(StaticScripts.SELSTAGE == 3){
				if(StaticScripts.SELTYPE == 1){
					bloodEnergy = maxbloodEnergy = Mathf.Round(30/NormratioEnergy*OratioEnergy);
					attackDamage = 7/NormratioPower*OratioPower;
					m_dMaxSpeed = 7.5f/NormratioSpeed*OratioSpeed;
				}else if(StaticScripts.SELTYPE == 2){
					bloodEnergy = maxbloodEnergy = Mathf.Round(30/NormratioEnergy*DratioEnergy);
					attackDamage = 7/NormratioPower*DratioPower;
					m_dMaxSpeed = 7.5f/NormratioSpeed*DratioSpeed;
				}
				EntityLevel = 1;
				m_dMass = 5.5f;
				BloodEatEnergy = 4;
				EatEnergy = 60;
			}
			/*
			EntityLevel = 4;
			bloodEnergy = maxbloodEnergy = 100;
			attackDamage = 15;
			BloodEatEnergy = 15f;
			EatEnergy = 1000;
			m_dMaxSpeed = 10f;
			m_dMass = 10f;
			*/
			SeekPoint = GameObject.Find("UserMovePoint");
		}
		
	}
	void EntityUpdate(){
		if(gameObject.name.Equals("User")) CameraPosition(false, false);
		Vector3	SteeringForce = Calculate();
		if(gameObject.rigidbody.isKinematic) return;
	
		SteeringForce.y = 0;
		ManagingEnergy(SteeringForce);
		Vector3 acceleration = SteeringForce / m_dMass;
		gameObject.rigidbody.velocity += acceleration;
		gameObject.rigidbody.velocity = Truncate (gameObject.rigidbody.velocity, m_dMaxSpeed);
		Vector3 point = gameObject.rigidbody.velocity.normalized;
		point.y=0;
		if(point.magnitude>=1.0f)
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(point - Vector3.zero),1);
	}
	void EnergySetting(){
		float PreyMaxSpeedTime, PredetorMaxSpeedTime, Beta;
		if(gameObject.tag == "Level0"){
			Pursuer = GameObject.FindWithTag("Level4");
			 PreyMaxSpeedTime = m_dMass*m_dMaxSpeed/m_dMaxForce;
			 PredetorMaxSpeedTime = Pursuer.GetComponent<MovingEntity>().m_dMass
										*Pursuer.GetComponent<MovingEntity>().m_dMaxSpeed
										/Pursuer.GetComponent<MovingEntity>().m_dMaxForce;
			 Beta=PredetorMaxSpeedTime - PreyMaxSpeedTime;
			if(Beta<0) Beta=0;
			float PreyNeedEnergy = (m_dMass*0.5f)*
				(m_dMaxSpeed*m_dMaxSpeed)+
				(m_dMass*1.0f)* //마찰력
				(m_dMaxSpeed*(40.0f))/  //시작할때 둘의 거리
				(Pursuer.GetComponent<MovingEntity>().m_dMaxSpeed - m_dMaxSpeed)+
				((m_dMass*1.0f*Beta*m_dMaxSpeed*Pursuer.GetComponent<MovingEntity>().m_dMaxSpeed*0.5f)/
				((Pursuer.GetComponent<MovingEntity>().m_dMaxSpeed - m_dMaxSpeed)));
			MaximumEnergy = PreyNeedEnergy*Random.Range(10,30);
			CurrentEnergy = MaximumEnergy*3; //수정
			Pursuer = null;
			return;
		}
		Evader = GameObject.FindWithTag("Level0");
		PreyMaxSpeedTime = Evader.GetComponent<MovingEntity>().m_dMass * 
					Evader.GetComponent<MovingEntity>().m_dMaxSpeed/
					Evader.GetComponent<MovingEntity>().m_dMaxForce;
		PredetorMaxSpeedTime = m_dMass*m_dMaxSpeed/m_dMaxForce;
		Beta = PredetorMaxSpeedTime - PreyMaxSpeedTime;
		if(Beta<0)	Beta = 0;
		float NeedEnergy = (m_dMass*0.5f)*
				(m_dMaxSpeed*m_dMaxSpeed)+
				((m_dMass*1.0f)* //마찰력 설정에 대한 고찰
				(m_dMaxSpeed*10.0f)/  //시작할때 둘의 거리
				(m_dMaxSpeed-Evader.GetComponent<MovingEntity>().m_dMaxSpeed))+
				((m_dMass*1.0f*Beta*m_dMaxSpeed*Evader.GetComponent<MovingEntity>().m_dMaxSpeed*0.5f)/
				((m_dMaxSpeed-Evader.GetComponent<MovingEntity>().m_dMaxSpeed)));
		MaximumEnergy = NeedEnergy*Random.Range(7,10);
		if(gameObject.transform.name.Equals("User")){
			if(StaticScripts.SELTYPE == 1){
				MaximumEnergy = Mathf.Round(NeedEnergy*10f/3.5f*2.5f);
			}else if(StaticScripts.SELTYPE == 2){
				MaximumEnergy = Mathf.Round(NeedEnergy*10f/3.5f*3.5f);
			}
			
		}
		CurrentEnergy = MaximumEnergy;
	}
	void Start () {
		DecideEntityAttribute();
		EnergySetting();
		ChangeMode("Wander");
		WanderMaxTime=Random.Range(3,5);
		
	}
	
	bool ShakeMode = false;
	float ShakeTime = 0;
	//========================유저 영역
	public void CameraPosition(bool Isshake, bool IsCritical){
		GameObject mCamera;
		mCamera = GameObject.Find("Camera");
		if(Isshake){
			ShakeMode=true;
			ShakeTime=0;
		}
		if(ShakeMode){
			ShakeTime+=Time.deltaTime;
			if(ShakeTime>=0.3f)ShakeMode=false;
			Hashtable ht = new Hashtable();
			if(IsCritical){
				ht.Add("x", 2f);
		        ht.Add("y", 2f);
		        ht.Add("time", 0.3f);
			}else{
				ht.Add("x", 1f);
		        ht.Add("y", 1f);
		        ht.Add("time", 0.3f);
			}
			iTween.ShakePosition(mCamera.gameObject,ht);
		}else{
			Vector3 position = transform.position;
			position.y += 28.8f;
			position.z -= 22.6f;
			mCamera.transform.position = position;
		}
		
	}
	
	void ModeChangeCheck(){
		if(Death || Eatting ) return;
		
		if(attack && Evader != null){
			PursuerDamageCalculate(true); // true는 데미지 계산 false는 critical 계산
			return;	
		}
		
		distanceToAnimal = nearestAnimal = 999999;
		
		for(int i=0; i<5; i++){
			GameObject[] ALLAnimals = GameObject.FindGameObjectsWithTag("Level"+i);
			if(ALLAnimals.Length ==0) continue;
			int temp=0;
			if(ALLAnimals[0].name.Equals(gameObject.transform.name)){
				temp++;
				if(ALLAnimals.Length == 1) continue;
			}
			distanceToAnimal = Vector3.Distance (SeekPoint.transform.position,ALLAnimals[temp].transform.position);
			if(distanceToAnimal < nearestAnimal){
				nearestAnimal = distanceToAnimal;
				Evader = ALLAnimals[temp];
			}
			foreach(GameObject Animal in ALLAnimals){
				if(Animal.name.Equals(gameObject.transform.name))continue;
				distanceToAnimal = Vector3.Distance(SeekPoint.transform.position, Animal.transform.position);
				if(distanceToAnimal < nearestAnimal){
					nearestAnimal = distanceToAnimal;
					Evader = Animal;
				}
			}
			
		}
		if(nearestAnimal < 4.0f) {
			SeekPoint.transform.position = Evader.transform.position;
			ChangeMode("Pursuit");
			return;
		}
		Evader = null;
		
		if(Vector3.Distance(gameObject.transform.position, SeekPoint.transform.position)<1.0f){
			ChangeMode("Wander");
			return;
		}
		ChangeMode("Seek");
	}
	void Update () {
		if(gameObject.name.Equals("User")) ModeChangeCheck();
		else DecideAction();
		EntityUpdate();
	}
	void PursuerDamageCalculate(bool isEvade){
		attackTime+=Time.deltaTime;
		if(attackTime>=1.0f){
			
			if(Evader.name.Equals("User")){//추격자가 유저를 공격할때 유저는 당하는순간 싸워야한다
				Evader.GetComponent<MovingEntity>().Evader = gameObject;
				Evader.GetComponent<MovingEntity>().ChangeMode("Attack");
			}
			
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Evader.transform.position - transform.position),1);
			gameObject.GetComponent<AnimationController>().AttackCount++;
			float Critical = 1.0f;
			if(Random.Range(0,30)<=EntityLevel*2)Critical += Random.Range(0,2);
			if(Evader.name.Equals("User")){
				bool isCritical = (Critical>1.0f);
				Evader.GetComponent<MovingEntity>().CameraPosition(true, isCritical);
			}
			//공격형 모드일때 크리티컬을 조절
			if(gameObject.name.Equals("User") && GameObject.Find("Camera").GetComponent<SkillManager>().EmittionOn && StaticScripts.SELTYPE == 1){
				Critical += Random.Range(1,4);
			}
			
			//크리티컬시 이팩트
			if(gameObject.name.Equals("User") && Critical>1.0f){
				GameObject temp = (GameObject)Instantiate(CriticalEffact,Evader.transform.position, Evader.transform.rotation);	
			}
			
			float Damage = (int)Random.Range(attackDamage, attackDamage+3)*Critical;
			Evader.GetComponent<EnergyBar>().isAttacked=true;
			
			//방어형 이팩트
			if(Evader.name.Equals("User") && GameObject.Find("Camera").GetComponent<SkillManager>().EmittionOn && StaticScripts.SELTYPE == 2){
				GameObject temp = (GameObject)Instantiate(GameObject.Find("Camera").GetComponent<SkillManager>().DeffenceObj, Evader.transform.position, Evader.transform.rotation);
				Evader.GetComponent<MovingEntity>().bloodEnergy -= Damage/2;
				Evader.GetComponent<EnergyBar>().Damage = Damage/2;
			}else{
				Evader.GetComponent<MovingEntity>().bloodEnergy -= Damage;
				Evader.GetComponent<EnergyBar>().Damage = Damage;
			}
			
			attackTime = 0.0f;
			
			
			
			if(Vector3.Distance(gameObject.transform.position,GameObject.Find("User").transform.position)<15.0f){
					GameObject.Find("AnimalDeathSoundObject").GetComponent<SoundManager>().PlayAttackSound();	
			}
			
			if(Evader.GetComponent<MovingEntity>().bloodEnergy<=0){
				Evader.GetComponent<MovingEntity>().bloodEnergy=0;
				Evader.GetComponent<MovingEntity>().CurrentEnergy=0;
				Evader.GetComponent<MovingEntity>().ChangeMode("Death");
				if(bloodEnergy>0){
					if(Vector3.Distance(gameObject.transform.position,GameObject.Find("User").transform.position)<15.0f){
						GameObject.Find("AnimalDeathSoundObject").GetComponent<SoundManager>().AnimalDeathSound(Evader.name);
						GameObject.Find("AnimalEattingSoundObject").GetComponent<SoundManager>().AnimalEattingSound(gameObject.name);	
					}
					ChangeMode("Eatting");	
				}
			}
		}
		
	}
	void EvaderDamageCalculate(){
		gethitTime+=Time.deltaTime;
		
		if(gethitTime>=1.0f){
			if(gameObject.transform.name=="RABBIT"){
				ChangeMode("GetHit");
				return;
			}
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Pursuer.transform.position - transform.position),1);
			gethitTime=0.0f;
			float Critical = 1.0f;
			
			
			if(Random.Range(0,30)<=EntityLevel*2)Critical += Random.Range(0,2);
			if(Pursuer.name.Equals("User")){
				bool isCritical = (Critical>1.0f);
				Pursuer.GetComponent<MovingEntity>().CameraPosition(true, isCritical);
			}
			
			if(gameObject.name.Equals("User") && GameObject.Find("Camera").GetComponent<SkillManager>().EmittionOn && StaticScripts.SELTYPE == 1){
				Critical += Random.Range(1,4);
			}
			
			if(gameObject.name.Equals("User") && Critical>1.0f){
				GameObject temp = (GameObject)Instantiate(CriticalEffact,Pursuer.transform.position, Pursuer.transform.rotation);
			}
			
			float Damage = (int)Random.Range(attackDamage, attackDamage+3) * Critical;
			Pursuer.GetComponent<EnergyBar>().isAttacked=true;
			
			if(Pursuer.name.Equals("User") && GameObject.Find("Camera").GetComponent<SkillManager>().EmittionOn && StaticScripts.SELTYPE == 2){
				GameObject temp = (GameObject)Instantiate(GameObject.Find("Camera").GetComponent<SkillManager>().DeffenceObj, Pursuer.transform.position, Pursuer.transform.rotation);
				Pursuer.GetComponent<MovingEntity>().bloodEnergy -= Damage/2;
				Pursuer.GetComponent<EnergyBar>().Damage = Damage/2;
			}else{
				Pursuer.GetComponent<MovingEntity>().bloodEnergy -= Damage;
				Pursuer.GetComponent<EnergyBar>().Damage = Damage;
			}
			
			if(Vector3.Distance(gameObject.transform.position,GameObject.Find("User").transform.position)<15.0f){
					GameObject.Find("AnimalDeathSoundObject").GetComponent<SoundManager>().PlayAttackSound();	
			}
			
			if(Pursuer.GetComponent<MovingEntity>().bloodEnergy<=0){
				Pursuer.GetComponent<MovingEntity>().bloodEnergy=0;
				Pursuer.GetComponent<MovingEntity>().CurrentEnergy=0;
				Pursuer.GetComponent<MovingEntity>().ChangeMode("Death");
				if(bloodEnergy>0){
					if(Vector3.Distance(gameObject.transform.position,GameObject.Find("User").transform.position)<15.0f){
						GameObject.Find("AnimalDeathSoundObject").GetComponent<SoundManager>().AnimalDeathSound(Pursuer.name);
						GameObject.Find("AnimalEattingSoundObject").GetComponent<SoundManager>().AnimalEattingSound(gameObject.name);	
					}
					ChangeMode("Eatting");	
				}
			}
		}
	}
	void OnCollisionEnter(Collision collision){
		if(collision.transform.name.Equals("GameWorld") || collision.transform.name.Equals("Wall")) return;
		if(collision.transform.tag.Equals(gameObject.transform.tag) && !gameObject.name.Equals("User") && !collision.transform.name.Equals("User")) return;
		if(collision.gameObject.GetComponent<MovingEntity>().EntityLevel > EntityLevel) {
			Pursuer = collision.transform.gameObject;
			
			if(collision.transform.name.Equals("User")){
				StaticScripts.deather = gameObject.transform.name;
			}
			Evader=null;
		}
		if(collision.gameObject.GetComponent<MovingEntity>().EntityLevel < EntityLevel){
			Evader = collision.transform.gameObject;
			if(collision.transform.name.Equals("User")){
				StaticScripts.deather = gameObject.transform.name;
			}
			Pursuer=null; 
		}
		if(collision.gameObject.name.Equals("User") && collision.gameObject.GetComponent<MovingEntity>().EntityLevel==EntityLevel){
			collision.transform.gameObject.GetComponent<MovingEntity>().Pursuer = null;
			collision.transform.gameObject.GetComponent<MovingEntity>().Evader = gameObject;
			collision.transform.gameObject.GetComponent<MovingEntity>().ChangeMode("Attack");
			ChangeMode("Attack");
			Pursuer = null;
			Evader = collision.transform.gameObject;
			gameObject.rigidbody.isKinematic = true;
			Evader.rigidbody.isKinematic = true;
			StaticScripts.deather = gameObject.transform.name;
			PursuerDamageCalculate(Evader.transform.GetComponent<MovingEntity>().Evade); 
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Evader.transform.position - transform.position),1);
			return;
		}		
		if(collision.gameObject.GetComponent<MovingEntity>().EntityLevel == EntityLevel) return;
		if(Evader != null && collision.transform.name.Equals(Evader.name)){ //내가 추격자라면
			ChangeMode("Attack");
			Pursuer=null;
			gameObject.rigidbody.isKinematic = true;
			
			PursuerDamageCalculate(Evader.transform.GetComponent<MovingEntity>().Evade); 
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Evader.transform.position - transform.position),1);
			return;
		}
		if(Pursuer != null && collision.transform.name.Equals(Pursuer.name)){ //내가 도망자라면
			ChangeMode("GetHit");
			Evader = null;
			gameObject.rigidbody.isKinematic = true;
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Pursuer.transform.position - transform.position),1);
		}
	}
}
