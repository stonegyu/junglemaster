﻿using UnityEngine;
using System.Collections;

public class QwestScripts : MonoBehaviour {
	public GUISkin messageskin, autofindanimal;
	public string AnimalQwestName;
	int AnimalQwestLevel;
	public Texture2D[] AnimalPicture = new Texture2D[6];
	public Texture2D QuestPicture;
	bool MatchAnimalOn = false;
	string[] AllAnimalName = new string[6];
	int[] AllAnimalLevel = new int[6];
	public int ClearNumber;
	int SelCharacterLimitNum;
	float distanceTosameSpecies,nearestsameSpecies;
	bool AutoCharge = false;
	float AutoChargeTime=0;
	void NearestQwestAnimal(int EntityLevel, string EntityName){
		GameObject[] QwestSpecies = GameObject.FindGameObjectsWithTag("Level"+EntityLevel);
		GameObject NearQwestSpecies=null;
		distanceTosameSpecies = nearestsameSpecies =99999;
		if(QwestSpecies.Length >1){
			int Number = 0;
			if(QwestSpecies[Number].name.Equals(gameObject.name)){
				Number++;
			}
			while(!QwestSpecies[Number].name.Equals(EntityName))Number++;
			Debug.Log("QestSpecies : "+QwestSpecies[Number].name + "EntityName "+EntityName);
			distanceTosameSpecies = Vector3.Distance (transform.position,QwestSpecies[Number].transform.position);
			if(distanceTosameSpecies < nearestsameSpecies){
				nearestsameSpecies = distanceTosameSpecies;
				NearQwestSpecies = QwestSpecies[Number];
			}
			foreach(GameObject Species in QwestSpecies){
				if(!Species.name.Equals(gameObject.name) && Species.name.Equals(EntityName)){
					distanceTosameSpecies = Vector3.Distance(transform.position, Species.transform.position);
					if(distanceTosameSpecies < nearestsameSpecies){
						nearestsameSpecies = distanceTosameSpecies;
						NearQwestSpecies = Species;
					}
				}
			}
			if(NearQwestSpecies!=null)
				GameObject.Find("UserMovePoint").transform.position=NearQwestSpecies.transform.position;
		}	
	}
	public void QwestGUI(){
		GUI.skin=autofindanimal;
		GUI.DrawTexture(new Rect(3, Screen.height - Screen.width/10, 65, 64), QuestPicture);
		if(!AutoCharge){
			
			if(GUI.Button(new Rect(3, Screen.height - Screen.width/10-65, 65, 64),"")){
				NearestQwestAnimal(AnimalQwestLevel, AnimalQwestName);
				AutoCharge=true;
				GameObject.Find("SoundObject").GetComponent<SoundManager>().PlayButtonSound();
			}
		}else{
			if(GUI.Button(new Rect(3, Screen.height - Screen.width/10-65, 65, 64),(int)(60.0f-AutoChargeTime)+"")){
			}
		}
		
	}
	void AssignQwest(){
		int t=Random.Range(0,SelCharacterLimitNum);
		AnimalQwestName = AllAnimalName[t]; 
		AnimalQwestLevel = AllAnimalLevel[t];
		QuestPicture = AnimalPicture[t];
	}
	public void MatchAnimal(string EatAnimalName){
		if(EatAnimalName.Equals(AnimalQwestName) && MatchAnimalOn == false){
			ClearNumber++;
			gameObject.GetComponent<ManageRanking>().UpdateScore(100*ClearNumber);	
			
			gameObject.GetComponent<GameMessage>().AddMessage(ClearNumber+"콤보달성 "+(100*ClearNumber)+"점 추가획득 합니다", "UpRankSound");
			if(ClearNumber>=3){
				int t = 10-ClearNumber;
				if(t<=7) t=3;
				float rate = GameObject.Find("User").GetComponent<MovingEntity>().MaximumEnergy/(10-ClearNumber);
				GameObject.Find("User").GetComponent<MovingEntity>().CurrentEnergy += GameObject.Find("User").GetComponent<MovingEntity>().MaximumEnergy/rate;
				GameObject.Find("User").GetComponent<MovingEntity>().bloodEnergy += GameObject.Find("User").GetComponent<MovingEntity>().maxbloodEnergy/rate;
			}
			MatchAnimalOn = true;
			AssignQwest();
			return;
		}
		if(!EatAnimalName.Equals(AnimalQwestName) && MatchAnimalOn==false){
			ClearNumber=0;
			AssignQwest();
		}
	}
	// Use this for initialization
	void Start () {
		AllAnimalName[0] = "RABBIT";AllAnimalLevel[0] = 0;
		AllAnimalName[1] = "STAG";AllAnimalLevel[1] = 0;
		AllAnimalName[2] = "FOX";AllAnimalLevel[2] = 1;
		AllAnimalName[3] = "BOAR";AllAnimalLevel[3] = 2;
		AllAnimalName[4] = "STANDARD_WOLF";AllAnimalLevel[4] = 3;
		AllAnimalName[5] = "BEAR";AllAnimalLevel[5] = 4;
		SelCharacterLimitNum=0;
		if(StaticScripts.SELSTAGE==1)SelCharacterLimitNum=6;
		if(StaticScripts.SELSTAGE==2)SelCharacterLimitNum=3;
		if(StaticScripts.SELSTAGE==3)SelCharacterLimitNum=2;
		AssignQwest();
	}
	
	// Update is called once per frame
	float modechangeTime=0;
	void Update () {
		if(AutoCharge){
			AutoChargeTime+=Time.deltaTime;
			if(AutoChargeTime>60.0f){
				AutoChargeTime=0;
				AutoCharge=false;
			}
		}
		if(MatchAnimalOn){
			modechangeTime += Time.deltaTime;
			if(modechangeTime>1.5f){
				modechangeTime=0;
				MatchAnimalOn=false;
			}
		}
		
	}
}
