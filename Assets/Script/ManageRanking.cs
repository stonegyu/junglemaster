﻿using UnityEngine;
using System.Collections;

public class ManageRanking : MonoBehaviour {
	
	int RememberPosition;
	float ScoreTime=0;
	public int CurrentGameScore=0;
	public int CurrentRank;
	// Use this for initialization
	
	
	void SwitchRank(){
		if(CurrentRank==1)return;
		if(StaticScripts.SELSTAGE==1){
			int UpperScore = System.Convert.ToInt32(StaticScripts.Stage1Rank[RememberPosition,1]);
			if(CurrentGameScore>UpperScore){
				CurrentRank--;
				RememberPosition++;
				gameObject.GetComponent<GameMessage>().AddMessage("랭킹 "+CurrentRank+" 등극!","UpRankSound");
			}
		}else if(StaticScripts.SELSTAGE==2){
			int UpperScore = System.Convert.ToInt32(StaticScripts.Stage2Rank[RememberPosition,1]);
			if(CurrentGameScore>UpperScore){
				CurrentRank--;
				RememberPosition++;
				gameObject.GetComponent<GameMessage>().AddMessage("랭킹 "+CurrentRank+" 등극!","UpRankSound");
			}
		}else if(StaticScripts.SELSTAGE==3){
			int UpperScore = System.Convert.ToInt32(StaticScripts.Stage3Rank[RememberPosition,1]);
			if(CurrentGameScore>UpperScore){
				CurrentRank--;
				RememberPosition++;
				gameObject.GetComponent<GameMessage>().AddMessage("랭킹 "+CurrentRank+" 등극!","UpRankSound");
			}
		}
		
	}
	
	
	void RankDisplayUpdate(){
		if(StaticScripts.SELSTAGE==1){
			if(CurrentRank==1){
				GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRank = CurrentRank;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRankerID = StaticScripts.MyID;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UpperScore = CurrentGameScore;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UserRank = StaticScripts.Stage1Rank.Length/2-RememberPosition+2;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UserID = StaticScripts.Stage1Rank[RememberPosition-1,0];
				int userScore = System.Convert.ToInt32(StaticScripts.Stage1Rank[RememberPosition-1,1]);
				GameObject.Find("Camera").GetComponent<DisplayUI>().UserScore = userScore;
				return;
			}
			GameObject.Find("Camera").GetComponent<DisplayUI>().UserRank = CurrentRank;
			GameObject.Find("Camera").GetComponent<DisplayUI>().UserScore = CurrentGameScore;
			GameObject.Find("Camera").GetComponent<DisplayUI>().UserID = StaticScripts.MyID;
			
			GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRank =StaticScripts.Stage1Rank.Length/2-RememberPosition;
			GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRankerID = StaticScripts.Stage1Rank[RememberPosition,0];
			int upperScore = System.Convert.ToInt32(StaticScripts.Stage1Rank[RememberPosition,1]);
			GameObject.Find("Camera").GetComponent<DisplayUI>().UpperScore = upperScore;
		}
		if(StaticScripts.SELSTAGE==2){
			if(CurrentRank==1){
				GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRank = CurrentRank;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRankerID = StaticScripts.MyID;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UpperScore = CurrentGameScore;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UserRank = StaticScripts.Stage2Rank.Length/2-RememberPosition+2;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UserID = StaticScripts.Stage2Rank[RememberPosition-1,0];
				int userScore = System.Convert.ToInt32(StaticScripts.Stage2Rank[RememberPosition-1,1]);
				GameObject.Find("Camera").GetComponent<DisplayUI>().UserScore = userScore;
				return;
			}
			GameObject.Find("Camera").GetComponent<DisplayUI>().UserRank = CurrentRank;
			GameObject.Find("Camera").GetComponent<DisplayUI>().UserScore = CurrentGameScore;
			GameObject.Find("Camera").GetComponent<DisplayUI>().UserID = StaticScripts.MyID;
			
			GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRank =StaticScripts.Stage2Rank.Length/2-RememberPosition;
			GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRankerID = StaticScripts.Stage2Rank[RememberPosition,0];
			int upperScore = System.Convert.ToInt32(StaticScripts.Stage2Rank[RememberPosition,1]);
			GameObject.Find("Camera").GetComponent<DisplayUI>().UpperScore = upperScore;
		}
		if(StaticScripts.SELSTAGE==3){
			if(CurrentRank==1){
				GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRank = CurrentRank;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRankerID = StaticScripts.MyID;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UpperScore = CurrentGameScore;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UserRank = StaticScripts.Stage3Rank.Length/2-RememberPosition+2;
				GameObject.Find("Camera").GetComponent<DisplayUI>().UserID = StaticScripts.Stage3Rank[RememberPosition-1,0];
				int userScore = System.Convert.ToInt32(StaticScripts.Stage3Rank[RememberPosition-1,1]);
				GameObject.Find("Camera").GetComponent<DisplayUI>().UserScore = userScore;
				return;
			}
			GameObject.Find("Camera").GetComponent<DisplayUI>().UserRank = CurrentRank;
			GameObject.Find("Camera").GetComponent<DisplayUI>().UserScore = CurrentGameScore;
			GameObject.Find("Camera").GetComponent<DisplayUI>().UserID = StaticScripts.MyID;
			
			GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRank =StaticScripts.Stage3Rank.Length/2-RememberPosition;
			GameObject.Find("Camera").GetComponent<DisplayUI>().UpperRankerID = StaticScripts.Stage3Rank[RememberPosition,0];
			int upperScore = System.Convert.ToInt32(StaticScripts.Stage3Rank[RememberPosition,1]);
			GameObject.Find("Camera").GetComponent<DisplayUI>().UpperScore = upperScore;
		}
		
	}
	
	
	public void UpdateScore(int Score){
		if(Score>=100){
			Debug.Log("score  "+Score);
			gameObject.GetComponent<ScoreEffect>().ScoreEffectOn(Score);	
		}
		CurrentGameScore += Score; 
		RankDisplayUpdate();
		SwitchRank();
	}
	
	void Start () {
		if(StaticScripts.SELSTAGE==1)CurrentRank = StaticScripts.Stage1Rank.Length/2+1;
		if(StaticScripts.SELSTAGE==2)CurrentRank = StaticScripts.Stage2Rank.Length/2+1;
		if(StaticScripts.SELSTAGE==3)CurrentRank = StaticScripts.Stage3Rank.Length/2+1;
		RememberPosition=0;
	}
	
	// Update is called once per frame
	void Update () {
		ScoreTime += Time.deltaTime;
		if(ScoreTime > 0.1f){
			ScoreTime=0;
			if(!GameObject.Find("User").GetComponent<MovingEntity>().Death)
				UpdateScore(1);
		}
	}
}
