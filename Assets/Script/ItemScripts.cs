﻿using UnityEngine;
using System.Collections;

public class ItemScripts : MonoBehaviour {
	public int Stamina = 0, Energy = 0;
	public void itemManage(){
		int temp = Random.Range(0,2);
		if(temp==0){
			Stamina++;
			GameObject.Find("Camera").GetComponent<GameMessage>().AddMessage("스테미너 아이템을 획득 하였습니다","GetItemSound");
		}else if(temp==1){
			Energy++;
			GameObject.Find("Camera").GetComponent<GameMessage>().AddMessage("에너지 아이템을 획득 하였습니다","GetItemSound");
		}
		
		
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
