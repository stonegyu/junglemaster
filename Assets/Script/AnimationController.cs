﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {
	public int WanderCount = 0;
	public int AttackCount = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	public void AnimationUpdate(){
		if(gameObject.GetComponent<MovingEntity>().attack){
			if(gameObject.name.Equals("RABBIT") || gameObject.name.Equals("RABBIT(Clone)")){
				gameObject.animation.Play("idle1");return;
			}
			if(AttackCount % 2 == 0)gameObject.animation.Play("attack1");
			else gameObject.animation.Play("attack2");
			return;
		}
		if(gameObject.GetComponent<MovingEntity>().Eatting){
			if(gameObject.name.Equals("RABBIT")||
				gameObject.name.Equals("FOX")||
				gameObject.name.Equals("STAG")){
				gameObject.animation.Play("idle1");
				return;
			}
			if(gameObject.name.Equals("User") && gameObject.GetComponent<MovingEntity>().EntityLevel==1){
				gameObject.animation.Play("attack1");
				return;
			}
			gameObject.animation.Play("eatting");
			return;
		}
		if(gameObject.GetComponent<MovingEntity>().getHit){
			if(gameObject.name.Equals("RABBIT") || gameObject.name.Equals("RABBIT(Clone)")) {
				gameObject.animation.Play("idle1"); return;
			}
			gameObject.animation.Play("gethit");
			return;
		}
		if(gameObject.GetComponent<MovingEntity>().Evade){
			gameObject.animation.Play("run");	
			return;
		}
		if(gameObject.GetComponent<MovingEntity>().Seek){
			if(gameObject.name.Equals("User")){
				if(GameObject.Find("Camera").GetComponent<MouseController>().RunAndWalkMode==false){
					gameObject.animation.Play("run");
					return;
				}
			}
			gameObject.animation.Play("walk");
			return;
		}
		if(gameObject.GetComponent<MovingEntity>().Wander){
			if(WanderCount%2==0)gameObject.animation.Play("idle1");
			else gameObject.animation.Play("idle2");
			return;
		}
		if(gameObject.GetComponent<MovingEntity>().Pursuit){
			gameObject.animation.Play("run");
			return;
		}
		if(gameObject.GetComponent<MovingEntity>().Death){
			gameObject.animation.Play("death");
			return;
		}
	}
	void Update () {
	
	}
}
